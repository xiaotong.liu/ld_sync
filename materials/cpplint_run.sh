#!/bin/bash
set -eu
root=".."
lint_dir=("${root}/app")

for d in "${lint_dir[*]}"
do
cpplint --recursive $d
done
