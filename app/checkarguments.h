/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef CHECKARGUMENTS_H
#define CHECKARGUMENTS_H

#include <iostream>

int checkArguments(const int argc, const char** argv, bool& hasLogFile) {
  //    const int minNbOfNeededArguments = 2;
  //    const int maxNbOfNeededArguments = 3;

  const int minNbOfNeededArguments = 5;
  const int maxNbOfNeededArguments = 11;

  bool wrongNbOfArguments = false;
  if (argc < minNbOfNeededArguments) {
    std::cerr << "Missing argument" << std::endl;
    wrongNbOfArguments = true;
  } else if (argc > maxNbOfNeededArguments) {
    std::cerr << "Too many argument" << std::endl;
    wrongNbOfArguments = true;
  }

  else if (argc < maxNbOfNeededArguments && argc > minNbOfNeededArguments) {
    std::cerr << "Confusing number of arguments" << std::endl;
    wrongNbOfArguments = true;
  }

  if (wrongNbOfArguments) {
    std::cerr << argv[0] << " "
              << " INPUTFILENAME [LOGFILE]" << std::endl;
    std::cerr << "\tINPUTFILENAME Name of the file to use as input instead of "
                 "a sensor."
              << std::endl;
    std::cerr << "\tLOGFILE name of the log file. If ommitted, the log output "
                 "will be performed to stderr."
              << std::endl;
    return 1;
  }

  hasLogFile = (true);
  return 0;
}

#endif  // CHECKARGUMENTS_H
