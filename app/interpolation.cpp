/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#include "interpolation.h"

interpolation::interpolation(
    std::vector<ros::Time>& iv_tStmp_,
    std::vector<ld_msgs::ld_imugps_can>& v_can_velocity_,
    std::vector<ld_msgs::ld_imugps_can>& v_can_yawrate_) {
  for (int idx_tIV = 0; idx_tIV < iv_tStmp_.size(); idx_tIV++) {
    for (int vel_idx = vel_crt_idx;
         vel_idx < v_can_velocity_.size() - size_limit; vel_idx++) {
      double chk = (iv_tStmp_.at(idx_tIV).toSec() -
                    v_can_velocity_.at(vel_idx).can_ts.toSec()) *
                   (iv_tStmp_.at(idx_tIV).toSec() -
                    v_can_velocity_.at(vel_idx + 1).can_ts.toSec());
      if (chk < 0) {
        //                double lengLeft = fabs(iv_tStmp_.at(idx_tIV) -
        //                v_can_velocity_.at(vel_idx).toSec()); double lengRight
        //                = fabs(iv_tStmp_.at(idx_tIV) -
        //                v_can_velocity_.at(vel_idx+1).toSec()); double vel_fl
        //                = (v_can_velocity_.at(vel_idx)*lengRight +
        //                v_can_velocity_.at(vel_idx+1)*lengLeft)/(lengLeft+lengRight);
        //                vec_velocity_fl.push_back(vel_fl);

        double delT_01 = v_can_velocity_.at(vel_idx).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx + 1).can_ts.toSec();
        double delT_02 = v_can_velocity_.at(vel_idx).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx + 2).can_ts.toSec();

        double delT_10 = v_can_velocity_.at(vel_idx + 1).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx).can_ts.toSec();
        double delT_12 = v_can_velocity_.at(vel_idx + 1).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx + 2).can_ts.toSec();

        double delT_20 = v_can_velocity_.at(vel_idx + 2).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx).can_ts.toSec();
        double delT_21 = v_can_velocity_.at(vel_idx + 2).can_ts.toSec() -
                         v_can_velocity_.at(vel_idx + 1).can_ts.toSec();

        double delT_g0 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_velocity_.at(vel_idx).can_ts.toSec();
        double delT_g1 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_velocity_.at(vel_idx + 1).can_ts.toSec();
        double delT_g2 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_velocity_.at(vel_idx + 2).can_ts.toSec();

        double vel_fq = v_can_velocity_.at(vel_idx).v_x_CAN *
                            (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                        v_can_velocity_.at(vel_idx + 1).v_x_CAN *
                            (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                        v_can_velocity_.at(vel_idx + 2).v_x_CAN *
                            (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_velocity_fq.push_back(vel_fq);

        vel_crt_idx = vel_idx;
        break;
      }
    }
    for (int yawrate_idx = yaw_crt_idx;
         yawrate_idx < v_can_yawrate_.size() - size_limit; yawrate_idx++) {
      double chk = (iv_tStmp_.at(idx_tIV).toSec() -
                    v_can_yawrate_.at(yawrate_idx).can_ts.toSec()) *
                   (iv_tStmp_.at(idx_tIV).toSec() -
                    v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec());

      if (chk < 0) {
        //                double lengLeft = fabs(iv_tStmp_.at(idx_tIV) -
        //                v_can_yawrate_.at(yawrate_idx).toSec()); double
        //                lengRight = fabs(iv_tStmp_.at(idx_tIV) -
        //                v_can_yawrate_.at(yawrate_idx+1).toSec()); double
        //                yaw_fl = (yawrate_rad.at(yawrate_idx)*lengRight +
        //                yawrate_rad.at(yawrate_idx+1)*lengLeft)/(lengLeft+lengRight);
        //                vec_yawrate_fl.push_back(yaw_fl);
        //                double accx_fl =
        //                (v_can_yawrate_.at(yawrate_idx)*lengRight +
        //                v_can_yawrate_.at(yawrate_idx+1)*lengLeft)/(lengLeft+lengRight);
        //                vec_accx_fl.push_back(accx_fl);
        //                double accy_fl =
        //                (yawrate_accy.at(yawrate_idx)*lengRight +
        //                yawrate_accy.at(yawrate_idx+1)*lengLeft)/(lengLeft+lengRight);
        //                vec_accy_fl.push_back(accy_fl);

        double delT_01 = v_can_yawrate_.at(yawrate_idx).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec();
        double delT_02 = v_can_yawrate_.at(yawrate_idx).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx + 2).can_ts.toSec();

        double delT_10 = v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx).can_ts.toSec();
        double delT_12 = v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx + 2).can_ts.toSec();

        double delT_20 = v_can_yawrate_.at(yawrate_idx + 2).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx).can_ts.toSec();
        double delT_21 = v_can_yawrate_.at(yawrate_idx + 2).can_ts.toSec() -
                         v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec();

        double delT_g0 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_yawrate_.at(yawrate_idx).can_ts.toSec();
        double delT_g1 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_yawrate_.at(yawrate_idx + 1).can_ts.toSec();
        double delT_g2 = iv_tStmp_.at(idx_tIV).toSec() -
                         v_can_yawrate_.at(yawrate_idx + 2).can_ts.toSec();

        double yawrate_fq = v_can_yawrate_.at(yawrate_idx).yawrate *
                                (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                            v_can_yawrate_.at(yawrate_idx + 1).yawrate *
                                (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                            v_can_yawrate_.at(yawrate_idx + 2).yawrate *
                                (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_yawrate_fq.push_back(yawrate_fq);

        double accx_fq = v_can_yawrate_.at(yawrate_idx).acceleration_x *
                             (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                         v_can_yawrate_.at(yawrate_idx + 1).acceleration_x *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         v_can_yawrate_.at(yawrate_idx + 2).acceleration_x *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accx_fq.push_back(accx_fq);

        double accy_fq = v_can_yawrate_.at(yawrate_idx).acceleration_y *
                             (delT_g1 * delT_g2) / (delT_01 * delT_02) +
                         v_can_yawrate_.at(yawrate_idx + 1).acceleration_y *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         v_can_yawrate_.at(yawrate_idx + 2).acceleration_y *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accy_fq.push_back(accy_fq);

        yaw_crt_idx = yawrate_idx;
        break;
      }
    }
  }
}
