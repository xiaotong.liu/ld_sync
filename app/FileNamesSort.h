/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/

#ifndef FILENAMESSORT_H_
#define FILENAMESSORT_H_

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void FileNamesSort(std::vector<std::string>& filenames) {
  std::sort(filenames.begin(), filenames.end());
  return;
}

#endif /* FILENAMESSORT_H_ */
