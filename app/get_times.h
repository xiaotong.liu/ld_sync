/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef GET_TIMES_H
#define GET_TIMES_H

#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

/* Description
To check that the period of CAN message covers
the period of innovusion point cloud, if it is
not the case, function will give negative feedback
*/

bool get_times(std::string pc2_file, std::string can_file, int& pc_start_time,
               int& pc_end_time) {
  rosbag::Bag pc2_bag(pc2_file);
  rosbag::Bag can_bag(can_file);

  const int boundary_limit = 5;

  int can_start_time, can_end_time;

  std::vector<std::string> topics;
  topics.push_back(std::string("/iv_points"));
  topics.push_back(std::string("/ld_can"));

  std::vector<ros::Time> pc2_timestamps;
  std::vector<ros::Time> can_timestamps;

  rosbag::View view(pc2_bag, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const m,
           view)  // A class pointing into a bag file.
  {
    sensor_msgs::PointCloud2::ConstPtr s =
        m.instantiate<sensor_msgs::PointCloud2>();
    if (m.getTopic() == "/iv_points") {
      pc2_timestamps.push_back(s->header.stamp);
    }
  }

  rosbag::View can_view(can_bag, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const n, can_view) {
    ld_msgs::ld_can::ConstPtr s = n.instantiate<ld_msgs::ld_can>();
    if (n.getTopic() == "/ld_can") {
      can_timestamps.push_back(s->header.stamp);
    }
  }

  can_start_time = (int)floor(can_timestamps.front().toSec());
  can_end_time = (int)floor(can_timestamps.back().toSec());

  pc_start_time = (int)floor(pc2_timestamps.front().toSec()) - boundary_limit;
  pc_end_time = (int)floor(pc2_timestamps.back().toSec()) + boundary_limit;

  if (pc_start_time <= can_start_time || pc_end_time >= can_end_time)
    return false;

  return true;
}

#endif  // GET_TIMES_H
