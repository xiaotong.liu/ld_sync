/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef IDC2BAG_FILE_DEMO_H
#define IDC2BAG_FILE_DEMO_H

#include "alllistener.h"

const std::string file_demo(const std::string& filename) {
  const std::string idc2rosbag_path = "idc2rosbag.bag";

  IdcFile file;
  file.open(filename);
  if (file.isOpen()) {
    AllListener allListener;

    file.registerListener(&allListener);

    const DataBlock* db = NULL;
    unsigned short nbMessages = 0;  // # of messages we parsed

    while (file.isGood()) {
      db = file.getNextDataBlock();
      if (db == NULL) {
        continue;  // might be eof or unknown file type
      }
      file.notifyListeners(db);
      ++nbMessages;
    }

    rosbag::Bag bag_write;

    bag_write.open(idc2rosbag_path, rosbag::bagmode::Write);

    std::vector<sensor_msgs::PointCloud2> ros_cloud;
    sensor_msgs::PointCloud2 ros_point;

    for (int i = 0; i < pcl_pointclouds.size(); i++) {
      pcl::toROSMsg(pcl_pointclouds[i], ros_point);

      ros_cloud.push_back(ros_point);
      ros_cloud[i].header.frame_id = "innovusion";
      ros_cloud[i].header.stamp = tr_scantimestamp[i];

      bag_write.write("/lux_pointcloud", tr_scantimestamp[i], ros_cloud[i]);
    }

    for (int j = 0; j < ObjectsList.size(); j = j + 2) {
      bag_write.write("/lux_ObjectList_yes", objtr_scantimestamp[j],
                      ObjectsList[j]);
      //		    std::cout << "written to rosbag " << j << std::endl;
    }
    for (int j = 1; j < ObjectsList.size(); j = j + 2) {
      bag_write.write("/lux_ObjectList_no", objtr_scantimestamp[j],
                      ObjectsList[j]);
    }

    std::cout << "EOF reached. " << nbMessages << " known blocks found."
              << std::endl;
  } else {
    std::cout << "File not readable." << std::endl;
  }

  return idc2rosbag_path;
}
#endif  // IDC2BAG_FILE_DEMO_H
