/*
 * main.cpp
 *
 *  Created on: May 16, 2019
 *      Author: xiaotongliu
 */

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace bg = boost::gregorian;
namespace bpt = boost::posix_time;

//#include "portable-file-dialogs.h"
//#include <nfd.h>
#include <stdio.h>

#include "FileNamesSort.h"

#include "alllistener.h"
#include "checkarguments.h"
#include "customlogstreamcallbackexample.h"
#include "get_times.h"
#include "getallformatfiles.h"
#include "idc2bag_file_demo.h"
#include "innocan_sync.h"
#include "innocan_time_bag.h"
#include "innocan_value.h"
#include "innocan_vel.h"
#include "interpolation_can.h"
#include "sync_idc.h"

//// libs for ibeo
//#include <ibeosdk/lux.hpp>
//#include <ibeosdk/ecu.hpp>
//#include <ibeosdk/minilux.hpp>
//#include <ibeosdk/scala.hpp>
//#include <ibeosdk/devices/IdcFile.hpp>
//#include <ibeosdk/datablocks/PointCloudPlane7510.hpp>
//#include <ibeosdk/datablocks/commands/CommandEcuAppBaseStatus.hpp>
//#include <ibeosdk/datablocks/commands/ReplyEcuAppBaseStatus.hpp>
//#include <ibeosdk/datablocks/commands/CommandEcuAppBaseCtrl.hpp>
//#include <ibeosdk/datablocks/commands/EmptyCommandReply.hpp>
//#include <ibeosdk/datablocks/CanMessage.hpp>
//#include<iomanip>

//// libs for rosbag API
//#include <rosbag/bag.h>
//#include <rosbag/view.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
//#include <sensor_msgs/PointCloud2.h>
//#include <std_msgs/Float64.h>
//#include "std_msgs/String.h"
//#include <std_msgs/Int32.h>
////#include <sensor_msgs/PointCloud2.h>

//// PCL library
//#include<pcl_conversions/pcl_conversions.h>
////#include<pcl/point_cloud.h>
////#include<pcl/ros/conversions.h>

//// Autoware msgs
//#include<autoware_msgs/DetectedObjectArray.h>
//#include<autoware_msgs/DetectedObject.h>

//#include <ld_msgs/ld_can.h>
//#include <ld_msgs/ld_imugps_can.h>

//#include <ld_msgs/ld_mobileye_lane.h>
//#include <ld_msgs/ld_mobileye_traffic_sign.h>

//#include <visualization_msgs/MarkerArray.h>

////#include <ld_data/frame/datablock/objectlist_2010.h>

//// Lib for

//#ifdef WITHJPEGSUPPORT
//# include <ibeosdk/jpegsupport/jmemio.h>
//#endif // WITHJPEGSUPPORT

//#ifdef WITHJPEGSUPPORTDEF
//#  include <ibeosdk/jpegsupport/jmemio.h>
//#endif

//#include <iostream>
//#include <cstdlib>

//#include <boost/foreach.hpp>
//#define foreach BOOST_FOREACH

//======================================================================
bool autocan_sync(std::string pc_file, std::string autocan_file,
                  std::string output_folder, std::string iv_bagfilename);

//======================================================================

// const ros::Time unixTime2RosTime(const time_t pUnixTime);
// time_t to_time_tUnix(boost::posix_time::ptime t);
// std::vector<ros::Time> canTimeStamps (const CanMessage* const canM);
// void syn_CanMsg(const std::vector<ros::Time> syned_idc_iv, const
// std::vector<ros::Time> canMsgTime); bool syn_everything(const std::string&
// iv_path, const std::string& idc_path, const std::string& opt_path,
// std::string output_filename);

//======================================================================

//======================================================================
//======================================================================
//======================================================================
class CustomLogStreamCallbackExample : public CustomLogStreamCallback {
 public:
  virtual ~CustomLogStreamCallbackExample() {}

 public:
  virtual void onLineEnd(const char* const s, const int) {
    std::cerr << s << std::endl;
  }
};  // CustomLogStreamCallback

//======================================================================
//======================================================================
//======================================================================

//======================================================================

int main(const int argc, const char** argv) {
  std::time_t beginning = std::time(nullptr);

  bool hasLogFile;
  const int checkResult = checkArguments(argc, argv, hasLogFile);
  if (checkResult != 0) exit(checkResult);
  int currArg = 1;

  std::string CAN_filename =
      argv[currArg++];  // argv[1]:autocan message from bag or idc file
  std::string iv_path = argv[currArg++];  // argv[2]: innovusion points cloud
  std::string output_folder = argv[currArg++];  // argv[3]: output folder

  //    ibeosdk::LogFile::setLogFileBaseName(argv[currArg++]);

  std::vector<float> paras;
  bool is_paras_given;

  if (argc == 11) {
    float tx, ty, tz, yawrate_x, yawrate_y, yawrate_z;

    std::string str_tx = argv[currArg++];
    std::string str_ty = argv[currArg++];
    std::string str_tz = argv[currArg++];
    std::string str_yawrate_x = argv[currArg++];
    std::string str_yawrate_y = argv[currArg++];
    std::string str_yawrate_z = argv[currArg++];

    tx = std::stof(str_tx);
    ty = std::stof(str_ty);
    tz = std::stof(str_tz);
    yawrate_x = std::stof(str_yawrate_x);
    yawrate_y = std::stof(str_yawrate_y);
    yawrate_z = std::stof(str_yawrate_z);

    paras.push_back(tx);
    paras.push_back(ty);
    paras.push_back(tz);
    paras.push_back(yawrate_x);
    paras.push_back(yawrate_y);
    paras.push_back(yawrate_z);

    is_paras_given = true;

    //        std::cout << "11" << std::endl;
  }

  else {
    paras.push_back(0);
    paras.push_back(0);
    paras.push_back(0);
    paras.push_back(0);
    paras.push_back(0);
    paras.push_back(0);
    is_paras_given = false;
    //        std::cout << "not 11" << std::endl;
  }

  const off_t maxLogFileSize = 1000000;

  LogFileManager logFileManager;
  ibeosdk::LogFile::setTargetFileSize(maxLogFileSize);

  if (hasLogFile) {
    ibeosdk::LogFile::setLogFileBaseName(
        argv[4]);  // for now, the 4th argument is always the title for log file
  }

  const ibeosdk::LogLevel ll = ibeosdk::logLevelFromString("Debug");
  ibeosdk::LogFile::setLogLevel(ll);

  static CustomLogStreamCallbackExample clsce;

  if (!hasLogFile) LogFile::setCustomLogStreamCallback(&clsce);

  logFileManager.start();

  if (hasLogFile) {
    logInfo << argv[0] << " Version " << appVersion.toString()
            << "  using IbeoSDK " << ibeoSDK.getVersion().toString()
            << std::endl;
  }

  const unsigned int found = CAN_filename.find_last_of(".");
  std::string CAN_file_type = CAN_filename.substr(found);

  std::string idc2rosbag_path;  // .idc file converted to .bag file

  std::vector<std::string> iv_bagfiles;  // innovusion files (paths) from the
                                         // folder for being processed
  std::vector<std::string>
      iv_bagfilenames;  // innovusion files (names) from the folder for
                        // post-processed file names

  /*Get all the file names from the folder*/
  GetAllFormatFiles(iv_path, iv_bagfiles, ".bag", iv_bagfilenames,
                    BAGfilenames);

  /* Sort files by names*/
  FileNamesSort(iv_bagfiles);
  FileNamesSort(iv_bagfilenames);

  std::cout << "Name list: " << std::endl;
  for (unsigned int i = 0; i < iv_bagfilenames.size(); i++)
    std::cout << iv_bagfilenames[i] << std::endl;

  std::vector<std::string> invalid_pc;

  if (CAN_file_type == ".idc") {
    std::cerr << argv[0] << " Version " << appVersion.toString();
    std::cerr << "  using IbeoSDK " << ibeoSDK.getVersion().toString()
              << std::endl;

    /*file_demo function converts idc file to bag file*/
    idc2rosbag_path = file_demo(CAN_filename);

    /*the loop runs through each innovusion files in the folder
      for synchronizing points cloud and other messages
      processed from IDC files*/
    for (int iv_idx = 0; iv_idx < iv_bagfiles.size(); iv_idx++) {
      bool is_pc_valid;
      is_pc_valid = syn_everything(
          (std::string)iv_bagfiles.at(iv_idx), idc2rosbag_path, output_folder,
          iv_bagfilenames.at(iv_idx), paras, is_paras_given);

      if (!is_pc_valid) {
        invalid_pc.push_back(iv_bagfilenames.at(iv_idx));
      } else {
        std::cout << iv_bagfilenames.at(iv_idx)
                  << "********Process ends********" << '\n';
      }
    }
  }
  /* input file type is (.bag) file or something else */
  else {
    if (CAN_file_type == ".bag") {
      for (int iv_idx = 0; iv_idx < iv_bagfiles.size(); iv_idx++) {
        bool is_pc_valid;
        is_pc_valid =
            autocan_sync((std::string)iv_bagfiles.at(iv_idx), CAN_filename,
                         output_folder, iv_bagfilenames.at(iv_idx));

        if (!is_pc_valid) {
          invalid_pc.push_back(iv_bagfilenames.at(iv_idx));
        } else {
          std::cout << iv_bagfilenames.at(iv_idx)
                    << "********Process ends********" << '\n';
        }
        //                std::cout << (std::string)iv_bagfiles.at(iv_idx) <<
        //                std::endl;
      }
    }

    else {
      std::cerr << "!!!!!!!!!!!!!!!WRONG INPUT FILE TYPE!!!!!!!!!!!!!!!"
                << std::endl;
      std::cerr << "!!!!!!!!!!!!!!!INPUT IS NEITHER (.idc) NOR (.bag) "
                   "FILE!!!!!!!!!!!!!!!"
                << std::endl;
      exit(0);
    }
  }

  std::time_t ending = std::time(nullptr);
  std::cout << "Start time: " << std::asctime(std::localtime(&beginning))
            << std::endl;
  std::cout << "End time: " << std::asctime(std::localtime(&ending))
            << std::endl;

  if (invalid_pc.size() > 0) {
    std::cout << "The following files were not processed:" << std::endl;
    for (int idx_pc = 0; idx_pc < invalid_pc.size(); idx_pc++) {
      std::cout << invalid_pc.at(idx_pc) << std::endl;
    }
  }

  iv_bagfiles.clear();
  iv_bagfiles.shrink_to_fit();
  iv_bagfilenames.clear();
  iv_bagfilenames.shrink_to_fit();

  invalid_pc.clear();
  invalid_pc.shrink_to_fit();

  v_can_yawrate.clear();
  v_can_yawrate.shrink_to_fit();

  v_can_velocity.clear();
  v_can_velocity.shrink_to_fit();

  pcl_pointclouds.clear();
  pcl_pointclouds.shrink_to_fit();

  tr_scantimestamp.clear();
  tr_scantimestamp.shrink_to_fit();

  objtr_scantimestamp.clear();
  objtr_scantimestamp.shrink_to_fit();

  ObjectsList.clear();
  ObjectsList.shrink_to_fit();

  Objects.clear();
  Objects.shrink_to_fit();

  BAGfilenames.clear();
  BAGfilenames.shrink_to_fit();

  paras.clear();
  paras.shrink_to_fit();

  exit(0);
}

bool autocan_sync(std::string pc_file, std::string autocan_file,
                  std::string output_folder, std::string iv_bagfilename) {
  bool is_time_valid;
  int start_time, end_time;
  is_time_valid = get_times(pc_file, autocan_file, start_time, end_time);

  if (!is_time_valid) return false;

  //    std::cout << "start_time" << start_time <<std::endl;
  //    std::cout << "end_time" << end_time <<std::endl;

  std::cout << std::endl;
  std::cout << iv_bagfilename << " starts to be processed." << std::endl;

  /*****Step 1******/
  std::string first_bag =
      innocan_time_bag(autocan_file, output_folder, start_time, end_time);

  /*****Step 2******/
  std::string sec_bag = innocan_vel(first_bag, output_folder);

  /*****Step 3******/
  std::vector<ros::Time> can_velocity_time;
  std::vector<double> can_vel_Vx;

  std::vector<ros::Time> yawrate_time;
  std::vector<double> yawrate_accx;
  std::vector<double> yawrate_accy;
  std::vector<double> yawrate_rad;

  std::string third_bag =
      innocan_value(sec_bag, output_folder, can_velocity_time, can_vel_Vx,
                    yawrate_time, yawrate_accx, yawrate_accy, yawrate_rad);
  //
  //
  //        std::cout << "can_velocity_time" << can_velocity_time.size() <<
  //        std::endl; std::cout << "can_vel_Vx" << can_vel_Vx.size() <<
  //        std::endl; std::cout << "yawrate_time" << yawrate_time.size() <<
  //        std::endl; std::cout << "yawrate_accx" << yawrate_accx.size() <<
  //        std::endl; std::cout << "yawrate_accy" << yawrate_accy.size() <<
  //        std::endl; std::cout << "yawrate_rad" << yawrate_rad.size() <<
  //        std::endl;
  //

  /*****Step 4******/
  int pc_counter;
  std::vector<ros::Time> final_time;
  std::vector<sensor_msgs::PointCloud2> pc_value;

  std::string fourth_bag =
      interpolation_can(third_bag, pc_file, output_folder, can_velocity_time,
                        can_vel_Vx, yawrate_time, yawrate_accx, yawrate_accy,
                        yawrate_rad, pc_counter, final_time, pc_value);

  //    std::cout << "final_time.size()" << final_time.size() << std::endl;
  //    std::cout << "pc_value.size()" << pc_value.size() << std::endl;

  /*****Step 5******/
  innocan_sync(fourth_bag, pc_file, output_folder, pc_counter, final_time,
               pc_value, iv_bagfilename);

  can_velocity_time.clear();
  can_velocity_time.shrink_to_fit();

  can_vel_Vx.clear();
  can_vel_Vx.shrink_to_fit();

  yawrate_time.clear();
  yawrate_time.shrink_to_fit();

  yawrate_accx.clear();
  yawrate_accx.shrink_to_fit();

  yawrate_accy.clear();
  yawrate_accy.shrink_to_fit();

  yawrate_rad.clear();
  yawrate_rad.shrink_to_fit();

  final_time.clear();
  final_time.shrink_to_fit();

  pc_value.clear();
  pc_value.shrink_to_fit();

  return true;
}
