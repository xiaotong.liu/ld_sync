/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef GETALLFORMATFILES_H
#define GETALLFORMATFILES_H

#include <dirent.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

void GetAllFormatFiles(std::string path, std::vector<std::string>& files,
                       std::string format,
                       std::vector<std::string>& output_filenames,
                       std::vector<std::string>& BAGfilenames) {
  // path = path.append("/");
  DIR* p_dir;
  const char* str = path.c_str();
  p_dir = opendir(str);
  if (p_dir == NULL) {
    std::cout << "can't open :" << path << std::endl;
  }
  struct dirent* p_dirent;
  while (p_dirent = readdir(p_dir)) {
    std::string tmpFileName = p_dirent->d_name;

    if (tmpFileName.find(format) != std::string::npos) {
      output_filenames.push_back(tmpFileName);
      files.push_back(path + "/" + tmpFileName);

      if (format == ".bag") {
        BAGfilenames.push_back(tmpFileName);
      }
    }
  }
  closedir(p_dir);
}

#endif  // GETALLFORMATFILES_H
