/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

class interpolation {
 public:
  interpolation(std::vector<ros::Time>& iv_tStmp_,
                std::vector<ld_msgs::ld_imugps_can>& v_can_velocity_,
                std::vector<ld_msgs::ld_imugps_can>& v_can_yawrate_);

  std::vector<double> get_vec_velocity_fq() { return vec_velocity_fq; }
  std::vector<double> get_vec_yawrate_fq() { return vec_yawrate_fq; }
  std::vector<double> get_vec_accx_fq() { return vec_accx_fq; }
  std::vector<double> get_vec_accy_fq() { return vec_accy_fq; }

 private:
  std::vector<double> vec_velocity_fq;
  std::vector<double> vec_yawrate_fq;
  std::vector<double> vec_accx_fq;
  std::vector<double> vec_accy_fq;

  /* Interpolation process reaches following two points so the upper limit has
   * to be set up */
  const int size_limit = 2;

  /* Current index in process for shortening iteration */
  int vel_crt_idx = 0;
  int yaw_crt_idx = 0;
};

#endif  // INTERPOLATION_H
