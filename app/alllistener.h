/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef ALLLISTENER_H
#define ALLLISTENER_H

#include "generalfuncs.h"

// libs for ibeo
#include <ibeosdk/datablocks/CanMessage.hpp>
#include <ibeosdk/datablocks/PointCloudPlane7510.hpp>
#include <ibeosdk/datablocks/commands/CommandEcuAppBaseCtrl.hpp>
#include <ibeosdk/datablocks/commands/CommandEcuAppBaseStatus.hpp>
#include <ibeosdk/datablocks/commands/EmptyCommandReply.hpp>
#include <ibeosdk/datablocks/commands/ReplyEcuAppBaseStatus.hpp>
#include <ibeosdk/devices/IdcFile.hpp>
#include <ibeosdk/ecu.hpp>
#include <ibeosdk/lux.hpp>
#include <ibeosdk/minilux.hpp>
#include <ibeosdk/scala.hpp>
#include <iomanip>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"

// PCL library
#include <pcl_conversions/pcl_conversions.h>

#include <pcl/common/transforms.h>

// Autoware msgs
#include <autoware_msgs/DetectedObject.h>
#include <autoware_msgs/DetectedObjectArray.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <visualization_msgs/MarkerArray.h>

#ifdef WITHJPEGSUPPORT
#include <ibeosdk/jpegsupport/jmemio.h>
#endif  // WITHJPEGSUPPORT

#ifdef WITHJPEGSUPPORTDEF
#include <ibeosdk/jpegsupport/jmemio.h>
#endif

#include <cstdlib>
#include <iostream>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

//======================================================================

using namespace ibeosdk;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
// typedef std::vector<ObjectEcuEt> ObjectVector;
typedef std::vector<ObjectEcuEtDyn> ObjectVector;

//======================================================================

const ibeosdk::Version::MajorVersion majorVersion(5);
const ibeosdk::Version::MinorVersion minorVersion(2);
const ibeosdk::Version::Revision revision(2);
const ibeosdk::Version::PatchLevel patchLevel;
const ibeosdk::Version::Build build;
const std::string info = "IbeoSdkFileDemo";

ibeosdk::Version appVersion(majorVersion, minorVersion, revision, patchLevel,
                            build, info);

IbeoSDK ibeoSDK;
// claim Global Variable for point Cloud

std::vector<pcl::PointCloud<pcl::PointXYZ> > pcl_pointclouds;
std::vector<ros::Time> tr_scantimestamp;
std::vector<ros::Time> objtr_scantimestamp;
ros::Time rs_time;
ros::Time objrs_time;

std::vector<autoware_msgs::DetectedObjectArray> ObjectsList;
std::vector<autoware_msgs::DetectedObject> Objects;

// std::vector<ld_data::ObjectList2010> ObjectsList_ld;

std::vector<std::string> BAGfilenames;

// ======================================================================

std::vector<ld_msgs::ld_imugps_can> v_can_velocity;
std::vector<ld_msgs::ld_imugps_can> v_can_yawrate;

// ======================================================================

TimeConversion tc;

// ======================================================================

class AllListener
    : public ibeosdk::DataListener<FrameEndSeparator>,
      public ibeosdk::DataListener<CanMessage>,
      public ibeosdk::DataListener<ScanLux>,
      public ibeosdk::DataListener<ScanEcu>,
      public ibeosdk::DataListener<Scan2208>,
      public ibeosdk::DataListener<ObjectListLux>,
      public ibeosdk::DataListener<ObjectListEcu>,
      //                    public ibeosdk::DataListener<ObjectListScala>,
      //                    public ibeosdk::DataListener<ObjectListScala2271>,
      public ibeosdk::DataListener<ObjectListEcuEt>,
      public ibeosdk::DataListener<ObjectListEcuEtDyn>,
      public ibeosdk::DataListener<RefObjectListEcuEtDyn>,
      public ibeosdk::DataListener<Image>,
      public ibeosdk::DataListener<PositionWgs84_2604>,
      public ibeosdk::DataListener<MeasurementList2821>,
      public ibeosdk::DataListener<VehicleStateBasicLux>,
      public ibeosdk::DataListener<VehicleStateBasicEcu2806>,
      public ibeosdk::DataListener<VehicleStateBasicEcu>,
      public ibeosdk::DataListener<ObjectAssociationList4001>,
      public ibeosdk::DataListener<DeviceStatus>,
      public ibeosdk::DataListener<DeviceStatus6303>,
      public ibeosdk::DataListener<LogMessageError>,
      public ibeosdk::DataListener<LogMessageWarning>,
      public ibeosdk::DataListener<LogMessageNote>,
      public ibeosdk::DataListener<LogMessageDebug>,
      public ibeosdk::DataListener<PointCloudPlane7510>,
      public ibeosdk::DataListener<TimeRecord9000>,
      public ibeosdk::DataListener<Odometry9002>,
      public ibeosdk::DataListener<GpsImu9001>,
      public ibeosdk::DataListener<SystemMonitoringCanStatus6700>,
      public ibeosdk::DataListener<SystemMonitoringDeviceStatus6701>,
      public ibeosdk::DataListener<SystemMonitoringSystemStatus6705> {
 public:
  virtual ~AllListener() {}

 public:
  // ========================================
  void onData(const FrameEndSeparator* const fes) {
    logInfo << std::setw(5) << fes->getSerializedSize() << " Bytes  "
            << "Frame received: # " << fes->getFrameId() << "  Frame time: "
            << tc.toString(fes->getHeaderNtpTime().toPtime()) << std::endl;
  }

  // ========================================
  void onData(const CanMessage* const canM) {
    logInfo << std::setw(5) << "Byte0: " << (int)canM->getData(canM->Byte1)
            << " Bytes  "
            << "  Frame time: "
            << tc.toString(canM->getTimestamp()
                               .toTimeDurationSinceEpoch()
                               .total_milliseconds() -
                           2208988800000)
            << "  Can Id:  " << canM->getCanId() << std::endl;

    int ID = canM->getCanId();
    int IDdec = (int)canM->getCanId();

    std::string IDhex = int_to_hex(IDdec);
    int data[8];

    ld_msgs::ld_imugps_can
        can_write_velocity;  //这里只会存速度，每次是覆盖速度之后再存，所以每次存的时候不会带上上一次的旧数据
    ld_msgs::ld_imugps_can can_write_yawrate_acc;

    double velocity_raw;
    double velocity_kmh;
    double velocity_ms;
    double yawrate_raw;
    double yawrate_without_vz;
    int yawrate_vz;
    double yawrate_degree;
    double yawrate_radian;
    double acc_x_raw;
    double acc_x;
    double acc_y_raw;
    double acc_y;
    const double kGravi = 9.8;
    const double kPi = 3.1415926;

    data[0] = (int)canM->getData(canM->Byte0);
    data[1] = (int)canM->getData(canM->Byte1);
    data[2] = (int)canM->getData(canM->Byte2);
    data[3] = (int)canM->getData(canM->Byte3);
    data[4] = (int)canM->getData(canM->Byte4);
    data[5] = (int)canM->getData(canM->Byte5);
    data[6] = (int)canM->getData(canM->Byte6);
    data[7] = (int)canM->getData(canM->Byte7);

    std::bitset<8> byte0(data[0]);
    std::bitset<8> byte1(data[1]);
    std::bitset<8> byte2(data[2]);
    std::bitset<8> byte3(data[3]);
    std::bitset<8> byte4(data[4]);
    std::bitset<8> byte5(data[5]);
    std::bitset<8> byte6(data[6]);
    std::bitset<8> byte7(data[7]);

    if (IDhex == "0xfd") {
      velocity_raw = (data[5] << 8 | data[4]);
      velocity_kmh = velocity_raw * 0.01;
      velocity_ms = kmph2mps(velocity_kmh);

      double temp_time =
          canM->getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() -
          2208988800000;
      temp_time = temp_time / 1000;
      rs_time.fromSec(temp_time);

      velocity_raw = (data[5] << 8 | data[4]);
      velocity_kmh = velocity_raw * 0.01;
      velocity_ms = kmph2mps(velocity_kmh);

      can_write_velocity.v_x_CAN = velocity_ms;
      //            can_write_velocity.can_ts=unixTime2RosTime(unixTime);
      can_write_velocity.can_ts = rs_time;
      can_write_velocity.header.frame_id = canM->getCanId();

      //            std::cout << "velocity_ms:" << velocity_ms << std::endl;

      v_can_velocity.push_back(can_write_velocity);
    }

    if (IDhex == "0x101") {
      yawrate_raw = (data[6] & 0x3f) << 8 | data[5];
      yawrate_without_vz = yawrate_raw * 0.01;
      yawrate_vz = (data[6] & 0x40) >> 6;
      yawrate_degree = yawrate_calculate(yawrate_vz, yawrate_without_vz);
      yawrate_radian = degree2radian(yawrate_degree);
      acc_x_raw = (data[4] & 0x3) << 8 | data[3];
      acc_x = acc_x_raw * 0.03125 - 16;
      acc_y_raw = data[2];
      acc_y = (acc_y_raw * 0.01 - 1.27) * kGravi;

      //            time_t unixTime =
      //            to_time_tUnix(canM->getTimestamp().toPtime());

      double temp_time =
          canM->getTimestamp().toTimeDurationSinceEpoch().total_milliseconds() -
          2208988800000;
      temp_time = temp_time / 1000;
      rs_time.fromSec(temp_time);

      //            can_write_yawrate_acc.can_ts=unixTime2RosTime(unixTime);
      can_write_yawrate_acc.can_ts = rs_time;
      can_write_yawrate_acc.header.frame_id = canM->getCanId();
      can_write_yawrate_acc.acceleration_x = acc_x;
      can_write_yawrate_acc.acceleration_y = acc_y;
      can_write_yawrate_acc.yawrate = yawrate_radian;

      //            std::cout << "yawrate_radian:" << yawrate_radian <<
      //            std::endl;

      v_can_yawrate.push_back(can_write_yawrate_acc);
    }
  }

  // ========================================
  void onData(const ScanLux* const scan) {
    logInfo << std::setw(5) << scan->getSerializedSize() << " Bytes  "
            << "ScanLux received: # " << scan->getScanNumber()
            << "  ScanStart: "
            << tc.toString(scan->getStartTimestamp().toPtime()) << std::endl;
  }

  //========================================
  void onData(const ScanEcu* const scan) {
    // Where we get point cloud information under current configuration
    logInfo << std::setw(5) << scan->getSerializedSize() << " Bytes  "
            << "ScanEcu received: # " << scan->getScanNumber()
            << "  #Pts: " << scan->getNumberOfScanPoints() << "  ScanStart: "
            << tc.toString(scan->getStartTimestamp().toPtime(), 3) << std::endl;
    double temp_time = scan->getStartTimestamp()
                           .toTimeDurationSinceEpoch()
                           .total_milliseconds() -
                       2208988800000;
    temp_time = temp_time / 1000;
    rs_time.fromSec(temp_time);
    tr_scantimestamp.push_back(rs_time);

    int num = scan->getNumberOfScanPoints();
    pcl::PointCloud<pcl::PointXYZ> pcl_pointcloud;
    std::vector<ScanPointEcu> vec = scan->getScanPoints();
    pcl_pointcloud.width = num;
    pcl_pointcloud.height = 1;
    pcl_pointcloud.is_dense = false;
    pcl_pointcloud.points.resize(pcl_pointcloud.width * pcl_pointcloud.height);
    for (int i = 0; i < num; i++) {
      //			 pf_points[i].X = vec[i].getPositionX();
      //			 pf_points[i].Y = vec[i].getPositionY();
      //			 pf_points[i].Z = vec[i].getPositionZ();
      //			 pcl_pointclouds.push_back(pf_points);
      //                        pcl_pointcloud.points[i].x =
      //                        vec[i].getPositionZ()-1.46;
      //                        pcl_pointcloud.points[i].y =
      //                        -vec[i].getPositionY();
      //                        pcl_pointcloud.points[i].z =
      //                        vec[i].getPositionX()-1.7;

      pcl_pointcloud.points[i].x = vec[i].getPositionX();
      pcl_pointcloud.points[i].y = vec[i].getPositionY();
      pcl_pointcloud.points[i].z = vec[i].getPositionZ();
      //                        pcl_pointcloud.points[i].intensity =
      //                        vec[i].getEchoPulseWidth()*100;

      //             if (i == num-1)
      //             {
      //                             std::cout << "on data x: " <<
      //                             pcl_pointcloud.points[i].x << std::endl;
      //                             std::cout << "on data y: " <<
      //                             pcl_pointcloud.points[i].y << std::endl;
      //                             std::cout << "on data z: " <<
      //                             pcl_pointcloud.points[i].z << std::endl;

      //                 std::cout << "vec[i].getPositionX() " <<
      //                 vec[i].getPositionX() << std::endl; std::cout <<
      //                 "vec[i].getPositionY() " << vec[i].getPositionY() <<
      //                 std::endl; std::cout << "vec[i].getPositionZ() " <<
      //                 vec[i].getPositionZ() << std::endl;
      //             }
    }
    //		std::cout << "Total Point Cloud size is " <<
    // pcl_pointcloud.size()
    //<< std::endl;
    //        std::cout << "Here comes an end !!!!!!!" << std::endl;
    pcl_pointclouds.push_back(pcl_pointcloud);

    // point cloud

    std::ofstream coordOfPts("Coordinates.txt");
    if (coordOfPts.is_open()) {
      for (int indexOfPt = 0; indexOfPt < scan->getNumberOfScanPoints();
           indexOfPt++) {
        coordOfPts << indexOfPt << " "
                   << scan->getScanPoints().at(indexOfPt).getPositionX() << " "
                   << scan->getScanPoints().at(indexOfPt).getPositionY() << " "
                   << scan->getScanPoints().at(indexOfPt).getPositionZ()
                   << "\n";
      }
      coordOfPts.close();
    } else
      std::cout << "Unable to open file.";
  }

  // ========================================
  void onData(const Scan2208* const scan) {
    logInfo << std::setw(5) << scan->getSerializedSize() << " Bytes  "
            << "Scan2208 received: # " << scan->getScanNumber()
            << "  #Pts: " << scan->getSubScans().at(0).getNbOfPoints()
            << "  ScanStart: "
            << tc.toString(
                   scan->getSubScans().at(0).getStartScanTimestamp().toPtime(),
                   3)
            << std::endl;
  }

  // ========================================
  void onData(const ObjectListLux* const objList) {
    logInfo << std::setw(5) << objList->getSerializedSize() << " Bytes  "
            << "ObjectListLux received: # " << objList->getNumberOfObjects()
            << std::endl;
  }

  // ========================================
  void onData(const ObjectListEcu* const objList) {
    logInfo << std::setw(5) << objList->getSerializedSize() << " Bytes  "
            << "ObjectListEcu received: # " << objList->getNumberOfObjects()
            << std::endl;
  }

  // ========================================
  void onData(const ObjectListEcuEt* const objList) {
    logInfo << std::setw(5) << objList->getSerializedSize() << " Bytes  "
            << "ObjectListEcUEts received: # " << objList->getNbOfObjects()
            << std::endl;
    std::cout << "we have " << objList->getNbOfObjects()
              << " Objects inside !!!" << std::endl;
    int NbObject = objList->getNbOfObjects();
    // header part
    double object_time = objList->getScanStartTimestamp()
                             .toTimeDurationSinceEpoch()
                             .total_milliseconds() -
                         2208988800000;
    object_time = object_time / 1000;
    objrs_time.fromSec(object_time);
    objtr_scantimestamp.push_back(objrs_time);
    autoware_msgs::DetectedObjectArray temp_objList;
    temp_objList.header.stamp = objrs_time;
    temp_objList.header.frame_id = "innovusion";

    // frame number part defines in the main parts

    //		Objects = objList->getObjects();
    //        autoware_msgs::DetectedObject temp_obj;
    //        ObjectVector ibeo_obj;
    //        ibeo_obj =  objList->getObjects();     //objList->getObjects();
    //        for (int j=0; j<NbObject; j++){
    //        // ------ID----------
    //        temp_obj.id = ibeo_obj[j].getObjectId();
    //        // ------LABEL----------
    //        switch(ibeo_obj[j].getClassification())
    //        {
    //        case 0:{
    //            temp_obj.class_label_pred = "Unclassified";
    //            break;
    //        }
    //        case 1:{
    //                temp_obj.class_label_pred = "UnknownSmall";
    //                break;
    //        }
    //        case 2:{
    //                temp_obj.class_label_pred = "UnknownBig";
    //                break;
    //        }
    //        case 3:{
    //                temp_obj.class_label_pred = "Pedestrian";
    //                break;
    //        }
    //        case 4:{
    //                temp_obj.class_label_pred = "Bike";
    //                break;
    //        }
    //        case 5:{
    //            temp_obj.class_label_pred = "Car";
    //            break;
    //        }
    //        case 6:{
    //            temp_obj.class_label_pred = "Truck";
    //            break;
    //        }
    //        case 12:{
    //            temp_obj.class_label_pred = "Underdriveable";
    //            break;
    //        }
    //        case 15:{
    //            temp_obj.class_label_pred = "Motorcycle";
    //            break;
    //        }
    //        case 17:{
    //            temp_obj.class_label_pred = "Cyclist";
    //            break;
    //        }
    //        }

    //        // ------POSE---------- raw version ---
    //        temp_obj.pose.position.x = ibeo_obj[j].getObjBoxCenter().getX();
    //        temp_obj.pose.position.y = ibeo_obj[j].getObjBoxCenter().getY();
    //        temp_obj.pose.position.z = 2;

    //        double yaw = ibeo_obj[j].getObjBoxOrientationSigma();
    //        double roll = 0;
    //        double pitch = 0;
    //        double cy = cos(yaw/2);
    //        double sy = sin(yaw/2);
    //        double cp = cos(pitch/2);
    //        double sp = sin(pitch/2);
    //        double cr = cos(roll/2);
    //        double sr = sin(roll/2);

    //        temp_obj.pose.orientation.w = cy * cp * cr + sy * sp * sr;
    //        temp_obj.pose.orientation.x = cy * cp * sr - sy * sp * cr;
    //        temp_obj.pose.orientation.y = sy * cp * sr + cy * sp * cr;
    //        temp_obj.pose.orientation.z = sy * cp * cr - cy * sp * sr;

    //        // ------DIMENSION---------- raw version ---
    //        temp_obj.dimensions.x = ibeo_obj[j].getObjBoxSize().getX();
    //        temp_obj.dimensions.y = ibeo_obj[j].getObjBoxSize().getY();
    //        temp_obj.dimensions.z = 2;

    //        // ------VELOCITY---------- raw version ---
    //        temp_obj.velocity.linear.x = ibeo_obj[j].getAbsVelocity().getX();
    //        temp_obj.velocity.linear.y = ibeo_obj[j].getAbsVelocity().getY();
    //        temp_obj.velocity.linear.z = 0;

    ////		std::cout << "id of this object is " << temp_obj.id <<
    ///"label of this
    /// object is " << temp_obj.label <<std::endl; /		temp_obj.id =
    /// objList->getObjects() /	    Objects.push_back(temp_obj);
    //        temp_objList.objects.push_back(temp_obj);
    //        }
    //        ObjectsList.push_back(temp_objList);
    //            logInfo << std::setw(5) << objList->getSerializedSize() << "
    //            Bytes  " << "ObjectListEcUEts received: # " <<
    //            objList->getNbOfObjects() << std::endl;
  }

  // ========================================
  void onData(const ObjectListEcuEtDyn* const objList) {
    logInfo << std::setw(5) << objList->getSerializedSize()
            << " Bytes!!!!!!!!!!  "
            << "ObjectListEcuEtDyn received: # " << objList->getNbOfObjects()
            << std::endl
            << "  ObjListId: " << toHex(objList->getObjectListId())
            << "  DevIntfVr: " << toHex(objList->getDeviceInterfaceVersion())
            << "  ObjListId: " << toHex(int(objList->getDeviceType()))
            << std::endl;

    int NbObject = objList->getNbOfObjects();
    // header part
    double object_time = objList->getTimestamp()
                             .toTimeDurationSinceEpoch()
                             .total_milliseconds() -
                         2208988800000;
    object_time = object_time / 1000;
    objrs_time.fromSec(object_time);
    objtr_scantimestamp.push_back(objrs_time);
    autoware_msgs::DetectedObjectArray temp_objList;
    temp_objList.header.stamp = objrs_time;
    //        std::cout << "Object List time is   " << objrs_time <<std::endl;
    temp_objList.header.frame_id = "innovusion";

    // frame number part defines in the main parts

    //		Objects = objList->getObjects();
    autoware_msgs::DetectedObject temp_obj;
    ObjectVector ibeo_obj;
    ibeo_obj = objList->getObjects();
    for (int j = 0; j < NbObject; j++) {
      // ------ID----------
      temp_obj.id = ibeo_obj[j].getObjectId();
      // ------LABEL----------
      switch (ibeo_obj[j].getClassification()) {
        case 0: {
          temp_obj.class_label_pred = "Unclassified";
          break;
        }
        case 1: {
          temp_obj.class_label_pred = "UnknownSmall";
          break;
        }
        case 2: {
          temp_obj.class_label_pred = "UnknownBig";
          break;
        }
        case 3: {
          temp_obj.class_label_pred = "Pedestrian";
          break;
        }
        case 4: {
          temp_obj.class_label_pred = "Bike";
          break;
        }
        case 5: {
          temp_obj.class_label_pred = "Car";
          break;
        }
        case 6: {
          temp_obj.class_label_pred = "Truck";
          break;
        }
        case 12: {
          temp_obj.class_label_pred = "Underdriveable";
          break;
        }
        case 15: {
          temp_obj.class_label_pred = "Motorcycle";
          break;
        }
        case 17: {
          temp_obj.class_label_pred = "Cyclist";
          break;
        }
      }

      // ------POSE---------- raw version ---
      temp_obj.pose.position.x = ibeo_obj[j].getObjectBoxCenter().getX() - 1.46;
      temp_obj.pose.position.y = ibeo_obj[j].getObjectBoxCenter().getY();
      temp_obj.pose.position.z = -0.7;

      temp_obj.jsk_pose.position.x =
          ibeo_obj[j].getObjectBoxCenter().getX() - 1.46;
      temp_obj.jsk_pose.position.y = ibeo_obj[j].getObjectBoxCenter().getY();
      temp_obj.jsk_pose.position.z = -0.7;

      double yaw = ibeo_obj[j].getYawRate();
      double roll = 0;
      double pitch = 0;
      double cy = cos(yaw / 2);
      double sy = sin(yaw / 2);
      double cp = cos(pitch / 2);
      double sp = sin(pitch / 2);
      double cr = cos(roll / 2);
      double sr = sin(roll / 2);

      temp_obj.pose.orientation.w = cy * cp * cr + sy * sp * sr;
      temp_obj.pose.orientation.x = cy * cp * sr - sy * sp * cr;
      temp_obj.pose.orientation.y = sy * cp * sr + cy * sp * cr;
      temp_obj.pose.orientation.z = sy * cp * cr - cy * sp * sr;

      temp_obj.jsk_pose.orientation.w = cy * cp * cr + sy * sp * sr;
      temp_obj.jsk_pose.orientation.x = cy * cp * sr - sy * sp * cr;
      temp_obj.jsk_pose.orientation.y = sy * cp * sr + cy * sp * cr;
      temp_obj.jsk_pose.orientation.z = sy * cp * cr - cy * sp * sr;

      // ------DIMENSION---------- raw version ---
      temp_obj.dimensions.x = ibeo_obj[j].getObjectBoxSize().getX();
      temp_obj.dimensions.y = ibeo_obj[j].getObjectBoxSize().getY();
      temp_obj.dimensions.z = ibeo_obj[j].getObjectHeight();

      // ------VELOCITY---------- raw version ---
      temp_obj.velocity.linear.x = ibeo_obj[j].getAbsoluteVelocity().getX();
      temp_obj.velocity.linear.y = ibeo_obj[j].getAbsoluteVelocity().getY();
      temp_obj.velocity.linear.z = 0;

      //		std::cout << "id of this object is " << temp_obj.id <<
      //"label of this object is " << temp_obj.label <<std::endl;
      // temp_obj.id = objList->getObjects() 	    Objects.push_back(temp_obj);
      temp_objList.objects.push_back(temp_obj);
    }
    ObjectsList.push_back(temp_objList);

    //***********ld_data object2010***************//

    //        //            logInfo << std::setw(5) <<
    //        objList->getSerializedSize() << " Bytes  " << "ObjectListEcUEts
    //        received: # " << objList->getNbOfObjects() << std::endl; logInfo
    //        << std::setw(5) << objList->getSerializedSize() << " Bytes  " <<
    //        "ObjectListEcUEts received: # " << objList->getNbOfObjects() <<
    //        std::endl; int NbObject_ld = objList->getNbOfObjects();
    //        //header part

    //        double object_time_ld =
    //        objList->getTimestamp().toTimeDurationSinceEpoch().total_milliseconds()
    //        - 2208988800000; object_time_ld = object_time_ld/ 1000;

    //        ld_data::ObjectList2010 temp_objList_ld;

    //        ld_data::Timestamp temp_ts_ld;
    //        temp_ts_ld.set_sec_us(objList->getTimestamp().getSeconds(),objList->getTimestamp().getFracSeconds());
    //        temp_objList_ld.set_mid_scan_time(temp_ts_ld);
    //        std::vector<ld_data::Object2010> objs_ld;
    //        ld_data::Object2010 temp_obj_ld;
    //        //        objs = temp_objList.get_objects();
    //        ObjectVector ibeo_obj_ld;
    //        ibeo_obj_ld = objList->getObjects();
    //        for(int j=0;j<NbObject_ld;j++){
    //            temp_obj_ld.set_id(ibeo_obj[j].getObjectId());
    //            ld_data::BoundingBox2010 temp_box_ld;
    //            // save velocity
    //            ld_data::Twist2010 temp_twist_ld;
    //            ld_data::Vector3f2010 velo_ld;
    //            velo_ld.set_x(ibeo_obj_ld[j].getAbsoluteVelocity().getX());
    //            velo_ld.set_y(ibeo_obj_ld[j].getAbsoluteVelocity().getY());
    //            velo_ld.set_z(0);
    //            temp_twist_ld.set_linear(velo_ld);
    //            temp_box_ld.set_velocity(temp_twist_ld);
    //            //            std::cout << "Inside paint Object, object
    //            velocity is right " <<
    //            temp_box.get_velocity().get_linear().get_x() << std::endl;
    //            // save dimension
    //            ld_data::Vector3f2010 dim_ld;
    //            dim_ld.set_x(ibeo_obj_ld[j].getObjectBoxSize().getX());
    //            dim_ld.set_y(ibeo_obj_ld[j].getObjectBoxSize().getY());
    //            dim_ld.set_z(0);
    //            temp_box_ld.set_dimensions(dim_ld);
    //            //            std::cout << "Inside paint Object, object
    //            dimension is right " << temp_box.get_dimensions().get_x() <<
    //            std::endl;
    //            // save pose
    //            ld_data::Pose2010 pos_ld;
    //            ld_data::Point3f2010 position_ld;
    //            ld_data::Quaternion2010 quin_ld;
    //            position_ld.set_x(ibeo_obj_ld[j].getObjectBoxCenter().getX());
    //            position_ld.set_y(ibeo_obj_ld[j].getObjectBoxCenter().getY());
    //            position_ld.set_z(2);
    //            double yaw_ld = ibeo_obj_ld[j].getYawRate();
    //            double roll_ld = 0;
    //            double pitch_ld = 0;
    //            double cy_ld = cos(yaw_ld*0.5);
    //            double sy_ld = sin(yaw_ld*0.5);
    //            double cp_ld = cos(pitch_ld*0.5);
    //            double sp_ld = sin(pitch_ld*0.5);
    //            double cr_ld = cos(roll_ld*0.5);
    //            double sr_ld = sin(roll_ld*0.5);
    //            quin_ld.set_w(cy_ld * cp_ld * cr_ld + sy_ld * sp_ld * sr_ld);
    //            quin_ld.set_x(cy_ld * cp_ld * sr_ld - sy_ld * sp_ld * cr_ld);
    //            quin_ld.set_y(sy_ld * cp_ld * sr_ld + cy_ld * sp_ld * cr_ld);
    //            quin_ld.set_z(sy_ld * cp_ld * cr_ld - cy_ld * sp_ld * sr_ld);
    //            pos_ld.set_ref_point(position_ld);
    //            pos_ld.set_orientation(quin_ld);
    //            temp_box_ld.set_clustered_pose(pos_ld);
    //            //            std::cout << "Inside paint Object, object pose
    //            is right " <<
    //            temp_box.get_clustered_pose().get_ref_point().get_x() <<
    //            std::endl;
    //            // define objs.bbox
    //            temp_obj_ld.set_boundingbox(temp_box_ld);
    //            objs_ld.push_back(temp_obj_ld);
    //            //            std::cout << "Inside paint Object, define objs
    //            bbox " << std::endl;
    //            // save label
    //            //                ld_data::LabelingInfo2010 obj_label;
    //            ld_data::ClsInfo2010 obj_label_ld;
    //            switch(ibeo_obj_ld[j].getClassification())
    //            {
    //            //            std::cout << "Inside paint Object, labeling is
    //            " << ibeo_obj[j].getClassification() << std::endl; case 0:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kNull);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 1:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kUnknownSmall);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 2:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kUnknownBig);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 3:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kPedestrian);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 4:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kBike);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 5:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kCar);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 6:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kTruck);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //                //                          case 12:{
    //                //                              temp_obj.class_label_pred
    //                = "Underdriveable";
    //                //                              break;
    //                //                          }
    //            case 15:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kMotorcycle);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            case 17:{
    //                obj_label_ld.set_label(ld_data::ClsInfo2010::Label::kCyclist);
    //                objs_ld[j].set_cls_info(obj_label_ld);
    //                break;
    //            }
    //            }

    //            //                temp_objList_ld.set_objects(objs_ld);
    //            //                ObjectList_ld.push_back(temp_objList_ld);
    //            temp_objList_ld.get_objects().push_back(temp_obj_ld);
    //        }

    //        ObjectsList_ld.push_back(temp_objList_ld);
  }

  //========================================
  void onData(const RefObjectListEcuEtDyn* const refObjList) {
    logInfo << std::setw(5) << refObjList->getSerializedSize() << " Bytes  "
            << "RefObjectListEcuEtDyn received: # "
            << refObjList->getNbOfObjects() << std::endl
            << "  ObjListId: " << toHex(refObjList->getObjectListId())
            << "  DevIntfVr: " << toHex(refObjList->getDeviceInterfaceVersion())
            << "  ObjListId: " << toHex(refObjList->getDeviceType())
            << std::endl;
  }

  // ========================================
  void onData(const Image* const image) {
    logInfo << std::setw(5) << image->getSerializedSize() << " Bytes  "
            << "Image received: time: "
            << tc.toString(image->getTimestamp().toPtime()) << std::endl;

#ifdef WITHJPEGSUPPORT
    if ((image->getFormat() == Image::JPEG) ||
        (image->getFormat() == Image::MJPEG)) {
      const char* rawBuffer;
      unsigned int compressedSize;

      const bool ok = image->getImageBuffer(rawBuffer, compressedSize);

      if (!ok) {
        return;
      }

      const unsigned int rgbBufferSize =
          (unsigned int)(image->getWidth() * image->getHeight() * 3);
      unsigned char* rgbBuffer = new unsigned char[size_t(rgbBufferSize)];

      unsigned int width = 0;
      unsigned int height = 0;
      // fill rgbBuffer, width and height
      const int retCode = readJpegFromMemory(
          rgbBuffer, &width, &height,
          reinterpret_cast<const unsigned char*>(rawBuffer), compressedSize);

      logDebug << "ok: " << ok << " retCode: " << retCode << " Size: " << width
               << " x " << height << std::endl;

      {
        unsigned int cprSize;
        unsigned char* cprBuffer = new unsigned char[rgbBufferSize];
        writeJpegToMemory(rgbBuffer, width, height, cprBuffer, rgbBufferSize,
                          100, &cprSize);

        FILE* f = fopen("jpeg.jpg", "wb");
        fwrite(cprBuffer, cprSize, 1, f);
        delete[] cprBuffer;
      }

      delete[] rgbBuffer;
    }   // if image is JPEG or MJPEG
#endif  // WITHJPEGSUPPORT
  }

  // ========================================
  void onData(const PositionWgs84_2604* const wgs84) {
    logInfo << std::setw(5) << wgs84->getSerializedSize() << " Bytes  "
            << "PositionWGS84 received: time: "
            << tc.toString(wgs84->getPosition().getTimestamp().toPtime())
            << std::endl;
  }

  // ========================================
  void onData(const VehicleStateBasicLux* const vsb) {
    logInfo << std::setw(5) << vsb->getSerializedSize() << " Bytes  "
            << "VSB (LUX) received: time: "
            << tc.toString(vsb->getTimestamp().toPtime()) << std::endl;
  }

  // ========================================
  void onData(const VehicleStateBasicEcu2806* const vsb) {
    logInfo << std::setw(5) << vsb->getSerializedSize() << " Bytes  "
            << "VSB (ECU;old) received: time: "
            << tc.toString(vsb->getTimestamp().toPtime()) << std::endl;
  }

  // ========================================
  void onData(const VehicleStateBasicEcu* const vsb) {
    logInfo << std::setw(5) << vsb->getSerializedSize() << " Bytes  "
            << "VSB (ECU) received: time: "
            << tc.toString(vsb->getTimestamp().toPtime()) << std::endl;
  }
  // ========================================
  void onData(const MeasurementList2821* const ml) {
    logInfo << std::setw(5) << ml->getSerializedSize() << " Bytes  "
            << "MeasurementList received: time: "
            << tc.toString(ml->getTimestamp().toPtime()) << " LN: '"
            << ml->getListName() << "' GN: '" << ml->getGroupName() << "'"
            << "Num: " << ml->getMeasList().getSize() << std::endl;

    typedef std::vector<Measurement> MLVector;

    MLVector::const_iterator itMl = ml->getMeasList().getMeasurements().begin();
    int ctr = 0;
    for (; itMl != ml->getMeasList().getMeasurements().end(); ++itMl, ++ctr) {
      logInfo << " M" << ctr << ":" << (*itMl) << std::endl;
    }
  }

  // ========================================
  void onData(const ObjectAssociationList4001* const oaList) {
    logInfo << std::setw(5) << oaList->getSerializedSize() << " Bytes  "
            << "ObjectAssociationList4001 received" << std::endl
            << "  RObjListId: " << toHex(oaList->getRefObjListId())
            << "  RDevIntfVr: " << toHex(oaList->getRefDevInterfaceVersion())
            << "  DevType: " << toHex(oaList->getRefDevType()) << std::endl
            << "  DObjListId: " << toHex(oaList->getDutObjListId())
            << "  DDevIntfVr: " << toHex(oaList->getDutDevInterfaceVersion())
            << "  DevType: " << toHex(oaList->getDutDevType())
            << "  # of associations: " << oaList->getObjectAssociations().size()
            << std::endl;
  }

  // ========================================
  void onData(const DeviceStatus* const devStat) {
    logInfo << std::setw(5) << devStat->getSerializedSize() << " Bytes  "
            << "DevStat received" << std::endl;
  }

  // ========================================
  void onData(const DeviceStatus6303* const devStat) {
    logInfo << std::setw(5) << devStat->getSerializedSize() << " Bytes  "
            << "DevStat 0x6303 received" << std::endl;
  }

  // ========================================
  void onData(const LogMessageError* const logMsg) {
    logInfo << std::setw(5) << logMsg->getSerializedSize() << " Bytes  "
            << "LogMessage (Error) received: time: " << logMsg->getTraceLevel()
            << ": " << logMsg->getMessage() << std::endl;
  }

  // ========================================
  void onData(const LogMessageWarning* const logMsg) {
    logInfo << std::setw(5) << logMsg->getSerializedSize() << " Bytes  "
            << "LogMessage (Warning) received: time: "
            << logMsg->getTraceLevel() << ": " << logMsg->getMessage()
            << std::endl;
  }

  // ========================================
  void onData(const LogMessageNote* const logMsg) {
    logInfo << std::setw(5) << logMsg->getSerializedSize() << " Bytes  "
            << "LogMessage (Note) received: time: " << logMsg->getTraceLevel()
            << ": " << logMsg->getMessage() << std::endl;
  }

  // ========================================
  void onData(const LogMessageDebug* const logMsg) {
    logInfo << std::setw(5) << logMsg->getSerializedSize() << " Bytes  "
            << "LogMessage (Debug) received: time: " << logMsg->getTraceLevel()
            << ": " << logMsg->getMessage() << std::endl;
  }

  // ========================================
  void onData(const PointCloudPlane7510* const pcl) {
    logInfo << std::setw(5) << pcl->getSerializedSize() << " Bytes  "
            << "PointCloudPlane7510 received. Is empty: " << pcl->empty()
            << "  ReferencePlane at position: "
            << pcl->getReferencePlane().getGpsPoint().getLatitudeInDeg() << "  "
            << pcl->getReferencePlane().getGpsPoint().getLongitudeInDeg()
            << std::endl;
  }

  // ========================================
  void onData(const TimeRecord9000* const tim) {
    logInfo << std::setw(5) << tim->getSerializedSize() << " Bytes  "
            << "Internal Clock Times Received with size: "
            << tim->getInternalClockTimes().size()
            << "External Clock Times Received with size: "
            << tim->getExternalClockTimes().size() << std::endl;
  }
  // ========================================
  void onData(const Odometry9002* const odo) {
    logInfo << std::setw(5) << odo->getSerializedSize() << " Bytes  "
            << "Steering angle: " << odo->getSteeringAngle() << std::endl;
  }
  // ========================================
  void onData(const GpsImu9001* const gpsImu) {
    logInfo << std::setw(5) << gpsImu->getSerializedSize() << " Bytes  "
            << "GpsImu9001 received: time: "
            << tc.toString(
                   gpsImu->getTimestamp().getReceivedTimeEcu().toPtime())
            << " "
            << "Source: " << GpsImu9001::toString(gpsImu->getSource()) << "  "
            << std::endl;
  }
  // ========================================
  void onData(const SystemMonitoringCanStatus6700* const canStatus) {
    logInfo << std::setw(5) << canStatus->getSerializedSize() << " Bytes  "
            << "SystemMonitoringCANStatus6700 received: "
            << canStatus->toString() << "  " << std::endl;
  }
  // ========================================
  void onData(const SystemMonitoringDeviceStatus6701* const deviceStatus) {
    logInfo << std::setw(5) << deviceStatus->getSerializedSize() << " Bytes  "
            << "SystemMonitoringDeviceStatus6701 received: "
            << deviceStatus->toString() << "  " << std::endl;
  }
  // ========================================
  void onData(const SystemMonitoringSystemStatus6705* const systemStatus) {
    logInfo << std::setw(5) << systemStatus->getSerializedSize() << " Bytes  "
            << "SystemMonitoringSystemStatus6705 received: "
            << systemStatus->toString() << "  " << std::endl;
  }
  // ========================================
};  // AllListener

#endif  // ALLLISTENER_H
