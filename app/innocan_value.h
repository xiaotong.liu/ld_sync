/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INNOCAN_VALUE_H
#define INNOCAN_VALUE_H

#include <iostream>
#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include <bitset>

#include "generalfuncs.h"

/*
 * Tasks:
 * value of Velocity, Yawrate, Vorzeichen Yawrate, Acc in X, Acc in Y
 * Velocity in /velocity topic
 * yawrate and acc in /yawrate_acc topic
 */

// template< typename T >
// std::string int_to_hex( T i )
//{
//  std::stringstream stream;
//  stream << "0x"<< std::hex<< i;
//  return stream.str();
//}

// double yawrate_calculate(int& yawrate_vz,double& yawrate_without_vz) {
//    switch(yawrate_vz) {
//        case 0: return yawrate_without_vz;
//        case 1: return -1*yawrate_without_vz;
//    }
//}

// double kmph2mps(double& kmph){
//    return kmph*1000/3600;
//}

// double degree2radian(double& degree){
//    const double kPi=3.1415926;
//    return degree/180*kPi;
//}

std::string innocan_value(
    std::string bagname_original, std::string bagname_write,
    std::vector<ros::Time>& can_vel_time, std::vector<double>& can_vel_Vx,
    std::vector<ros::Time>& yawrate_time, std::vector<double>& yawrate_accx,
    std::vector<double>& yawrate_accy, std::vector<double>& yawrate_rad) {
  bagname_write = bagname_write + "/3-5.bag";

  /**************** here to change files *************************/
  //	std::string bagname_original =
  //			"/home/chinyen/Documents/syn/2-5.bag";
  //	std::string bagname_write =
  //			"/home/chinyen/Documents/syn/3-5.bag";
  /**************** here to change files *************************/

  std::cout << "Step 3 starts" << std::endl;

  rosbag::Bag bag(bagname_original);
  rosbag::Bag bag_write(bagname_write, rosbag::bagmode::Write);

  std::cout << "the size of the bag is: " << bag.getSize() << '\n';
  std::cout << "now open the bag\n";

  int number = 0;
  int data[8];

  ld_msgs::ld_imugps_can
      can_write_velocity;  //这里只会存速度，每次是覆盖速度之后再存，所以每次存的时候不会带上上一次的旧数据
  ld_msgs::ld_imugps_can can_write_yawrate_acc;

  //	std::fstream write_test;
  //	write_test.open("/media/han/Transcend/Demo
  // VW/syn/temp_can/3value/t1-value.csv",std::ios::out);
  //
  //	write_test<<"number"<<","<<"time"<<","<<"byte7"<<","<<"byte6"<<","<<"byte5"<<","<<"byte4"<<","<<"byte3"<<","
  //			<<"byte2"<<","<<"byte1"<<","<<"byte0"<<","<<"name"<<","<<"value"<<","
  //			<<"name"<<","<<"value"<<","
  //			<<"name"<<","<<"value"<<","<<"name"<<","<<"value"<<","<<"name"<<","<<"value"<<","
  //			<<'\n';
  double velocity_raw;
  double velocity_kmh;
  double velocity_ms;
  double yawrate_raw;
  double yawrate_without_vz;
  int yawrate_vz;
  double yawrate_degree;
  double yawrate_radian;
  double acc_x_raw;
  double acc_x;
  double acc_y_raw;
  double acc_y;
  const double kGravi = 9.8;
  const double kPi = 3.1415926;

  std::vector<std::string> topics;
  topics.push_back(std::string("/ld_can"));
  rosbag::View view(bag, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const m,
           view)  // A class pointing into a bag file.
  {
    ld_msgs::ld_can::ConstPtr s = m.instantiate<ld_msgs::ld_can>();
    if (m.getTopic() == "/ld_can") {
      data[0] = (int)s->DATA[0];
      data[1] = (int)s->DATA[1];
      data[2] = (int)s->DATA[2];
      data[3] = (int)s->DATA[3];
      data[4] = (int)s->DATA[4];
      data[5] = (int)s->DATA[5];
      data[6] = (int)s->DATA[6];
      data[7] = (int)s->DATA[7];
      std::bitset<8> byte0(data[0]);
      std::bitset<8> byte1(data[1]);
      std::bitset<8> byte2(data[2]);
      std::bitset<8> byte3(data[3]);
      std::bitset<8> byte4(data[4]);
      std::bitset<8> byte5(data[5]);
      std::bitset<8> byte6(data[6]);
      std::bitset<8> byte7(data[7]);
      std_msgs::Header header = s->header;
      int IDdec = (int)s->ID;
      std::string IDhex = int_to_hex(IDdec);
      if (IDhex == "0xfd") {
        ++number;
        velocity_raw = (data[5] << 8 | data[4]);
        velocity_kmh = velocity_raw * 0.01;
        velocity_ms = kmph2mps(velocity_kmh);

        can_write_velocity.v_x_CAN = velocity_ms;
        can_write_velocity.can_ts = header.stamp;
        can_write_velocity.header.frame_id = header.frame_id;

        can_vel_time.push_back(header.stamp);
        can_vel_Vx.push_back(velocity_ms);

        bag_write.write("/velocity", header.stamp, can_write_velocity);

        //                std::cout<<number<<'\n';
        //					write_test<<number<<","<<header.stamp<<","<<byte7<<","<<byte6<<","<<byte5<<","<<byte4<<","<<byte3<<","
        //							<<byte2<<","<<byte1<<","<<byte0<<","<<"velocity_raw"<<","<<velocity_raw<<","
        //							<<"velocity_kmh"<<","<<velocity_kmh<<","<<"velocity_ms"<<","<<velocity_ms<<'\n';
      }

      if (IDhex == "0x101") {
        ++number;
        yawrate_raw = (data[6] & 0x3f) << 8 | data[5];
        yawrate_without_vz = yawrate_raw * 0.01;
        yawrate_vz = (data[6] & 0x40) >> 6;
        yawrate_degree = yawrate_calculate(yawrate_vz, yawrate_without_vz);
        yawrate_radian = degree2radian(yawrate_degree);
        acc_x_raw = (data[4] & 0x3) << 8 | data[3];
        acc_x = acc_x_raw * 0.03125 - 16;
        acc_y_raw = data[2];
        acc_y = (acc_y_raw * 0.01 - 1.27) * kGravi;

        can_write_yawrate_acc.can_ts = header.stamp;
        can_write_yawrate_acc.header.frame_id = header.frame_id;
        can_write_yawrate_acc.acceleration_x = acc_x;
        can_write_yawrate_acc.acceleration_y = acc_y;
        can_write_yawrate_acc.yawrate = yawrate_radian;

        yawrate_time.push_back(header.stamp);
        yawrate_accx.push_back(acc_x);
        yawrate_accy.push_back(acc_y);
        yawrate_rad.push_back(yawrate_radian);

        //					std::cout << "yawrate_radian" <<
        // yawrate_radian << std::endl;

        bag_write.write("/yawrate_acc", header.stamp, can_write_yawrate_acc);

        //                std::cout<<number<<'\n';
        //					write_test<<number<<","<<header.stamp<<","<<byte7<<","<<byte6<<","<<byte5<<","
        //							<<byte4<<","<<byte3<<","<<byte2<<","<<byte1<<","<<byte0<<","
        //							<<"yawrate_raw"<<","<<yawrate_raw<<","
        //							<<"yawrate_vz"<<","<<yawrate_vz<<","
        //							<<"yawrate_degree"<<","<<yawrate_degree<<","<<"yawrate_radian"<<","<<yawrate_radian<<","
        //							<<"acc_x_raw"<<","<<acc_x_raw<<","<<"acc_y_raw"<<","<<acc_y_raw<<","
        //							<<'\n';
      }
    }
  }

  topics.clear();
  topics.shrink_to_fit();
  std::cout << "Step 3 finished" << std::endl;
  std::cout << "----------------------- " << '\n';
  bag.close();
  bag_write.close();

  return bagname_write;

  //	interpolation_can(bagname_write, inno_bagName);
}

#endif  // INNOCAN_VALUE_H
