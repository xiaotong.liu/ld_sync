/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INTERPOLATION_CAN_H
#define INTERPOLATION_CAN_H

#include <iostream>
#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

std::string interpolation_can(
    const std::string rawdata, const std::string& original_innovu,
    std::string output, std::vector<ros::Time>& can_vel_time,
    std::vector<double>& can_vel_Vx, std::vector<ros::Time>& yawrate_time,
    std::vector<double>& yawrate_accx, std::vector<double>& yawrate_accy,
    std::vector<double>& yawrate_rad, int& pc_counter,
    std::vector<ros::Time>& finalTime,
    std::vector<sensor_msgs::PointCloud2>& pc_value) {
  std::cout << "Step 4 starts" << std::endl;
  output = output + "/4-5.bag";

  //	std::vector<double> velocity_data;
  //	std::vector<double> velocity_time;
  //	std::vector<ros::Time> yawrate_time;
  //	std::vector<double> yawrate_data;
  //	std::vector<double> accx_data;
  //	std::vector<double> accy_data;

  //	# ======== change here ======== #
  //	rawdata = rosbag::Bag.open("/home/chinyen/Documents/syn/3-5.bag");
  //	# ======== change here ======== #
  //	bag_inno =
  // rosbag.Bag('/media/chinyen/IBEOFAT32/chengnuo/20190503130317_12.bag') #
  //======== change here ======== # 	bag_write =
  // rosbag.Bag('/home/chinyen/Documents/syn/4-5.bag', 'w')

  rosbag::Bag bag_rawdata(rawdata);
  rosbag::Bag bag_inno(original_innovu);
  rosbag::Bag bag_write;
  bag_write.open(output, rosbag::bagmode::Write);

  std::vector<std::string> canTopics;
  canTopics.push_back("/velocity");
  canTopics.push_back("/yawrate_acc");

  //	char frame_id;

  ld_msgs::ld_imugps_can can_write_yawrate_acc;

  rosbag::View canView(bag_rawdata, rosbag::TopicQuery(canTopics));

  foreach (rosbag::MessageInstance const m, canView) {
    ld_msgs::ld_imugps_can::ConstPtr s =
        m.instantiate<ld_msgs::ld_imugps_can>();

    if (m.getTopic() == "/velocity") {
      //			velocity_data.push_back(s->v_x_CAN);
      //			velocity_time.push_back(s->header.stamp.toSec());
      //			std::cout << "velocity_time at 4" <<
      // s->header.stamp.toSec() << std::endl;
      can_write_yawrate_acc.header.frame_id = s->header.frame_id;
    }
    if (m.getTopic() == "/yawrate_acc") {
      //
      //			yawrate_time.push_back(s->header.stamp);
      //			yawrate_data.push_back(s->yawrate);
      //			accx_data.push_back(s->acceleration_x);
      //			accy_data.push_back(s->acceleration_y);

      //			std::cout << "yawrate_time at 4-" <<
      // s->header.stamp.toSec() << std::endl; 			std::cout <<
      // "yawrate_data at 4-"
      //<< s->yawrate << std::endl; 			std::cout << "accx_data
      // at 4-" <<
      // s->acceleration_x << std::endl; 			std::cout <<
      // "accy_data at 4-" << s->acceleration_y << std::endl;
    }
  }
  canTopics.clear();
  canTopics.shrink_to_fit();

  bag_rawdata.close();

  //	std::vector<ros::Time> finalTime;
  std::vector<double> time_inno;
  std::vector<std::string> ivTopics;
  ivTopics.push_back("/iv_points");

  rosbag::View ivView(bag_inno, rosbag::TopicQuery(ivTopics));

  foreach (rosbag::MessageInstance const m, ivView) {
    sensor_msgs::PointCloud2::ConstPtr s =
        m.instantiate<sensor_msgs::PointCloud2>();

    if (s->header.stamp > can_vel_time.at(0) &&
        s->header.stamp < can_vel_time.at(can_vel_time.size() - 1) &&
        s->header.stamp > yawrate_time.at(0) &&
        s->header.stamp < yawrate_time.at(yawrate_time.size() - 1)) {
      time_inno.push_back(s->header.stamp.toSec());
      finalTime.push_back(s->header.stamp);
      pc_value.push_back(*s);
    }
  }

  bag_inno.close();
  ivTopics.clear();
  ivTopics.shrink_to_fit();

  //    std::cout << "interpo6" << std::endl;

  pc_counter = 0;
  while (time_inno.at(pc_counter) < can_vel_time.at(0).toSec()) {
    finalTime.erase(finalTime.begin());
    pc_counter++;
  }
  //
  //	if (time_inno.at(0) < yawrate_time.at(0).toSec())
  //	{
  //		finalTime.erase(finalTime.begin());
  //	}
  //
  //	if (time_inno.back() > yawrate_time.back().toSec())
  //		{
  //			finalTime.pop_back();
  //		}

  std::vector<double> vec_velocity_fl;
  std::vector<double> vec_yawrate_fl;
  std::vector<double> vec_accx_fl;
  std::vector<double> vec_accy_fl;

  std::vector<double> vec_velocity_fq;
  std::vector<double> vec_yawrate_fq;
  std::vector<double> vec_accx_fq;
  std::vector<double> vec_accy_fq;

  const int size_limit = 2;

  int vel_crt_idx = 0;
  int yaw_crt_idx = 0;

  for (int idx_tIV = 0; idx_tIV < time_inno.size(); idx_tIV++) {
    for (int vel_idx = vel_crt_idx; vel_idx < can_vel_time.size() - size_limit;
         vel_idx++) {
      double chk =
          (time_inno.at(idx_tIV) - can_vel_time.at(vel_idx).toSec()) *
          (time_inno.at(idx_tIV) - can_vel_time.at(vel_idx + 1).toSec());
      double chk2 =
          (time_inno.at(idx_tIV) - can_vel_time.at(vel_idx).toSec()) *
          (time_inno.at(idx_tIV) - can_vel_time.at(vel_idx + 2).toSec());
      if (chk < 0 || chk2 < 0) {
        double lengLeft =
            fabs(time_inno.at(idx_tIV) - can_vel_time.at(vel_idx).toSec());
        double lengRight =
            fabs(time_inno.at(idx_tIV) - can_vel_time.at(vel_idx + 1).toSec());
        double vel_fl = (can_vel_Vx.at(vel_idx) * lengRight +
                         can_vel_Vx.at(vel_idx + 1) * lengLeft) /
                        (lengLeft + lengRight);
        vec_velocity_fl.push_back(vel_fl);

        double delT_01 = can_vel_time.at(vel_idx).toSec() -
                         can_vel_time.at(vel_idx + 1).toSec();
        double delT_02 = can_vel_time.at(vel_idx).toSec() -
                         can_vel_time.at(vel_idx + 2).toSec();

        double delT_10 = can_vel_time.at(vel_idx + 1).toSec() -
                         can_vel_time.at(vel_idx).toSec();
        double delT_12 = can_vel_time.at(vel_idx + 1).toSec() -
                         can_vel_time.at(vel_idx + 2).toSec();

        double delT_20 = can_vel_time.at(vel_idx + 2).toSec() -
                         can_vel_time.at(vel_idx).toSec();
        double delT_21 = can_vel_time.at(vel_idx + 2).toSec() -
                         can_vel_time.at(vel_idx + 1).toSec();

        double delT_g0 =
            time_inno.at(idx_tIV) - can_vel_time.at(vel_idx).toSec();
        double delT_g1 =
            time_inno.at(idx_tIV) - can_vel_time.at(vel_idx + 1).toSec();
        double delT_g2 =
            time_inno.at(idx_tIV) - can_vel_time.at(vel_idx + 2).toSec();

        double vel_fq =
            can_vel_Vx.at(vel_idx) * (delT_g1 * delT_g2) / (delT_01 * delT_02) +
            can_vel_Vx.at(vel_idx + 1) * (delT_g0 * delT_g2) /
                (delT_10 * delT_12) +
            can_vel_Vx.at(vel_idx + 2) * (delT_g0 * delT_g1) /
                (delT_20 * delT_21);
        vec_velocity_fq.push_back(vel_fq);

        vel_crt_idx = vel_idx;
        break;
      }
    }
    for (int yawrate_idx = yaw_crt_idx;
         yawrate_idx < yawrate_time.size() - size_limit; yawrate_idx++) {
      double chk =
          (time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx).toSec()) *
          (time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx + 1).toSec());
      double chk2 =
          (time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx).toSec()) *
          (time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx + 2).toSec());

      if (chk < 0 || chk2 < 0) {
        double lengLeft =
            fabs(time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx).toSec());
        double lengRight = fabs(time_inno.at(idx_tIV) -
                                yawrate_time.at(yawrate_idx + 1).toSec());
        double yaw_fl = (yawrate_rad.at(yawrate_idx) * lengRight +
                         yawrate_rad.at(yawrate_idx + 1) * lengLeft) /
                        (lengLeft + lengRight);
        vec_yawrate_fl.push_back(yaw_fl);
        double accx_fl = (yawrate_accx.at(yawrate_idx) * lengRight +
                          yawrate_accx.at(yawrate_idx + 1) * lengLeft) /
                         (lengLeft + lengRight);
        vec_accx_fl.push_back(accx_fl);
        double accy_fl = (yawrate_accy.at(yawrate_idx) * lengRight +
                          yawrate_accy.at(yawrate_idx + 1) * lengLeft) /
                         (lengLeft + lengRight);
        vec_accy_fl.push_back(accy_fl);

        double delT_01 = yawrate_time.at(yawrate_idx).toSec() -
                         yawrate_time.at(yawrate_idx + 1).toSec();
        double delT_02 = yawrate_time.at(yawrate_idx).toSec() -
                         yawrate_time.at(yawrate_idx + 2).toSec();

        double delT_10 = yawrate_time.at(yawrate_idx + 1).toSec() -
                         yawrate_time.at(yawrate_idx).toSec();
        double delT_12 = yawrate_time.at(yawrate_idx + 1).toSec() -
                         yawrate_time.at(yawrate_idx + 2).toSec();

        double delT_20 = yawrate_time.at(yawrate_idx + 2).toSec() -
                         yawrate_time.at(yawrate_idx).toSec();
        double delT_21 = yawrate_time.at(yawrate_idx + 2).toSec() -
                         yawrate_time.at(yawrate_idx + 1).toSec();

        double delT_g0 =
            time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx).toSec();
        double delT_g1 =
            time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx + 1).toSec();
        double delT_g2 =
            time_inno.at(idx_tIV) - yawrate_time.at(yawrate_idx + 2).toSec();

        double yawrate_fq = yawrate_rad.at(yawrate_idx) * (delT_g1 * delT_g2) /
                                (delT_01 * delT_02) +
                            yawrate_rad.at(yawrate_idx + 1) *
                                (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                            yawrate_rad.at(yawrate_idx + 2) *
                                (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_yawrate_fq.push_back(yawrate_fq);

        double accx_fq = yawrate_accx.at(yawrate_idx) * (delT_g1 * delT_g2) /
                             (delT_01 * delT_02) +
                         yawrate_accx.at(yawrate_idx + 1) *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         yawrate_accx.at(yawrate_idx + 2) *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accx_fq.push_back(accx_fq);

        double accy_fq = yawrate_accy.at(yawrate_idx) * (delT_g1 * delT_g2) /
                             (delT_01 * delT_02) +
                         yawrate_accy.at(yawrate_idx + 1) *
                             (delT_g0 * delT_g2) / (delT_10 * delT_12) +
                         yawrate_accy.at(yawrate_idx + 2) *
                             (delT_g0 * delT_g1) / (delT_20 * delT_21);
        vec_accy_fq.push_back(accy_fq);

        yaw_crt_idx = yawrate_idx;
        break;
      }
    }
  }

  //        std::cout << "vec_velocity_fl size:" << vec_velocity_fl.size() <<
  //        std::endl;
  //	std::cout << "vec_yawrate_fl size:" << vec_yawrate_fl.size() <<
  // std::endl;
  //        std::cout << "vec_accx_fl size:" << vec_accx_fl.size() << std::endl;
  //        std::cout << "vec_accy_fl size:" << vec_accy_fl.size() << std::endl;
  //    std::cout << "vec_yawrate_fq size:" << vec_yawrate_fq.size() <<
  //    std::endl; std::cout << "vec_velocity_fq size:" <<
  //    vec_velocity_fq.size() << std::endl; std::cout << "time_inno size:" <<
  //    time_inno.size() << std::endl; std::cout << "finalTime size:" <<
  //    finalTime.size() << std::endl;
  //
  //	std::cout << "int frame_id--" << can_write_yawrate_acc.header.frame_id
  //<< std::endl; 	ld_msgs::ld_imugps_can can_write_yawrate_acc;

  //
  //	for (int i = 0; i < vec_velocity_fq.size(); i++)
  //	{
  //		std::cout << "vec_velocity_fq :" << vec_velocity_fq.at(i) <<
  // std::endl;
  //	}

  for (int idx_inno = 0; idx_inno < finalTime.size(); idx_inno++) {
    //		can_write_yawrate_acc.acceleration_x = vec_accx_fl.at(idx_inno);
    //		can_write_yawrate_acc.acceleration_y = vec_accy_fl.at(idx_inno);
    //		can_write_yawrate_acc.yawrate = vec_yawrate_fl.at(idx_inno);
    //		can_write_yawrate_acc.v_x_CAN = vec_velocity_fl.at(idx_inno);

    can_write_yawrate_acc.acceleration_x = vec_accx_fq.at(idx_inno);
    can_write_yawrate_acc.acceleration_y = vec_accy_fq.at(idx_inno);
    can_write_yawrate_acc.yawrate = vec_yawrate_fq.at(idx_inno);
    can_write_yawrate_acc.v_x_CAN = vec_velocity_fq.at(idx_inno);
    can_write_yawrate_acc.header.stamp = finalTime.at(idx_inno);

    bag_write.write("/ld_imugps_can", finalTime.at(idx_inno),
                    can_write_yawrate_acc);
    //		std::cout << finalTime.at(idx_inno) << std::endl;
  }

  time_inno.clear();
  time_inno.shrink_to_fit();

  vec_velocity_fl.clear();
  vec_velocity_fl.shrink_to_fit();

  vec_yawrate_fl.clear();
  vec_yawrate_fl.shrink_to_fit();

  vec_accx_fl.clear();
  vec_accx_fl.shrink_to_fit();

  vec_accy_fl.clear();
  vec_accy_fl.shrink_to_fit();

  vec_velocity_fq.clear();
  vec_velocity_fq.shrink_to_fit();

  vec_yawrate_fq.clear();
  vec_yawrate_fq.shrink_to_fit();

  vec_accx_fq.clear();
  vec_accx_fq.shrink_to_fit();

  vec_accy_fq.clear();
  vec_accy_fq.shrink_to_fit();

  bag_write.close();

  std::cout << "Step 4 finished" << std::endl;
  std::cout << "--------------------" << std::endl;
  return output;
  // inovan_syn(output, original_innovu);
}

#endif  // INTERPOLATION_CAN_H
