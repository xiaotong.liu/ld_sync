/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef TRANSFORM_COORDINATES_H
#define TRANSFORM_COORDINATES_H

// PCL library
#include <pcl_conversions/pcl_conversions.h>
//#include<pcl/point_cloud.h>
//#include<pcl/ros/conversions.h>

#include <pcl/common/transforms.h>

class transform_coordinates {
 public:
  //    transform_coordinates();
  transform_coordinates(float t_x_, float t_y_, float t_z_, float yawrate_x_,
                        float yawrate_y_, float yawrate_z_);
  void set_translation_mat();
  void set_rotation_mat();
  Eigen::Affine3f get_transform_mat() const { return transform_mat; };

 private:
  float theta = M_PI / 2;
  float t_x;
  float t_y;
  float t_z;
  float yawrate_x;
  float yawrate_y;
  float yawrate_z;
  Eigen::Affine3f transform_mat = Eigen::Affine3f::Identity();
};

#endif  // TRANSFORM_COORDINATES_H
