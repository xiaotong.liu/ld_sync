/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#include "transform_coordinates.h"

transform_coordinates::transform_coordinates(float t_x_, float t_y_, float t_z_,
                                             float yawrate_x_, float yawrate_y_,
                                             float yawrate_z_) {
  t_x = t_x_;
  t_y = t_y_;
  t_z = t_z_;

  yawrate_x = yawrate_x_;
  yawrate_y = yawrate_y_;
  yawrate_z = yawrate_z_;
  set_rotation_mat();
  set_translation_mat();
}

void transform_coordinates::set_translation_mat() {
  transform_mat.translation() << t_x, t_y, t_z;
  //    std::cout << "translate!" << std::endl;
}
void transform_coordinates::set_rotation_mat() {
  // transfrom coordinates from innovusion's to Ibeo's
  transform_mat.rotate(Eigen::AngleAxisf(theta, Eigen::Vector3f::UnitY()));
  transform_mat.rotate(Eigen::AngleAxisf(theta * 2, Eigen::Vector3f::UnitZ()));
  //    transform_mat.rotate (Eigen::AngleAxisf (theta/4,
  //    Eigen::Vector3f::UnitZ()));
  // rotate slightly with yawrate
  transform_mat.rotate(Eigen::AngleAxisf(yawrate_x, Eigen::Vector3f::UnitX()));
  transform_mat.rotate(Eigen::AngleAxisf(yawrate_y, Eigen::Vector3f::UnitY()));
  transform_mat.rotate(Eigen::AngleAxisf(yawrate_z, Eigen::Vector3f::UnitZ()));

  //    std::cout << "rotate!" << std::endl;
}
