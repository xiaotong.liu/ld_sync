/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INNOCAN_VEL_H
#define INNOCAN_VEL_H

#include <iostream>
#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include <bitset>

#include "generalfuncs.h"

/*
 * Tasks:
 * Rawdata of Velocity, Yawrate, Vorzeichen Yawrate, Acc in X, Acc in Y
 * Velocity: 0xfd
 * Yawrate,AccXY,VorzeichenYawrate:	0x101
 */

std::string innocan_vel(std::string bagname_original,
                        std::string bagname_write) {
  bagname_write = bagname_write + "/2-5.bag";
  /**************** here to change files *************************/
  //	std::string bagname_original =
  //			"/home/chinyen/Documents/syn/1-5.bag";
  //	std::string bagname_write =
  //			"/home/chinyen/Documents/syn/2-5.bag";

  /**************** here to change files *************************/
  std::cout << "Step 2 starts" << std::endl;
  std::cout << "now open the bag\n";
  /*input file*/
  rosbag::Bag bag(bagname_original);
  /*output file*/
  rosbag::Bag bag_write(bagname_write, rosbag::bagmode::Write);

  std::cout << "the size of the bag is: " << bag.getSize() << '\n';

  int number = 0;
  int num_acc = 0;
  ld_msgs::ld_can can_write_address;

  //	std::fstream write_test;
  //	write_test.open("/media/han/Transcend/Demo
  // VW/syn/temp_can/2address/t1-address.csv",std::ios::out);
  //	write_test<<"time"<<","<<"fieldheaderseq"<<","<<"fieldheaderstamp"<<","<<"fieldheaderframe_id"<<","<<"fieldid"<<","<<"fieldLEN"<<","
  //			<<"data0"<<","<<"data1"<<","<<"data2"<<","<<"data3"<<","<<"data4"<<","<<"data5"<<","<<"data6"<<","<<"data7"<<'\n';

  const char* treffic_sign[] = {
      "0_standard_regular_10_kph", "1_standard_regular_20_kph",
      "2_standard_regular_30_kph", "3_standard_regular_40_kph",
      "4_standard_regular_50_kph", "5_standard_regular_60_kph",
      "6_standard_regular_70_kph", "7_standard_regular_80_kph",
      "8_standard_regular_90_kph", "9_standard_regular_100_kph",
      "10_standard_regular_110_kph", "11_standard_regular_120_kph",
      "12_standard_regular_130_kph", "13_standard_regular_140_kph", "14-?",
      "15-?", "16-?", "17-?", "18-?", "19-?",
      "20_standard_regular_end_restriction_of_number_eg_60_end_of_restriction",
      "21-?", "22-?", "23-?", "24-?", "25-?", "26-?", "27-?",
      "28_standard_electronic_10_kph", "29_standard_electronic_20_kph",
      "30_standard_electronic_30_kph", "31_standard_electronic_40_kph",
      "32_standard_electronic_50_kph", "33_standard_electronic_60_kph",
      "34_standard_electronic_70_kph", "35_standard_electronic_80_kph",
      "36_standard_electronic_90_kph", "37_standard_electronic_100_kph",
      "38_standard_electronic_110_kph", "39_standard_electronic_120_kph",
      "40_standard_electronic_130_kph", "41_standard_electronic_140_kph",
      "42-?", "43-?", "44-?", "45-?", "46-?", "47-?", "48-?", "49-?",
      "50_standard_electronic_end_restriction_of_number_eg_60_end_of_"
      "restriction",
      "51-?", "52-?", "53-?", "54-?", "55-?", "56-?", "57-?", "58-?", "59-?",
      "60-?", "61-?", "62-?", "63-?",
      "64_standard_regular_general_end_all_restriction;",
      "65_standard_electronic_general_end_all_restriction;", "66-?", "67-?",
      "68-?", "69-?", "70-?", "71-?", "72-?", "73-?", "74-?", "75-?", "76-?",
      "77-?", "78-?", "79-?", "80-?", "81-?", "82-?", "83-?", "84-?", "85-?",
      "86-?", "87-?", "88-?", "89-?", "90-?", "91-?", "92-?", "93-?", "94-?",
      "95-?", "96-?", "97-?", "98-?", "99-?", "100_standard_regular_5_kph",
      "101_standard_regular_15_kph", "102_standard_regular_25_kph",
      "103_standard_regular_35_kph", "104_standard_regular_45_kph",
      "105_standard_regular_55_kph", "106_standard_regular_65_kph",
      "107_standard_regular_75_kph", "108_standard_regular_85_kph",
      "109_standard_regular_95_kph", "110_standard_regular_105_kph",
      "111_standard_regular_115_kph", "112_standard_regular_125_kph",
      "113_standard_regular_135_kph", "114_standard_regular_145_kph",
      "115_standard_electronic_5_kph", "116_standard_electronic_15_kph",
      "117_standard_electronic_25_kph", "118_standard_electronic_35_kph",
      "119_standard_electronic_45_kph", "120_standard_electronic_55_kph",
      "121_standard_electronic_65_kph", "122_standard_electronic_75_kph",
      "123_standard_electronic_85_kph", "124_standard_electronic_95_kph",
      "125_standard_electronic_105_kph", "126_standard_electronic_115_kph",
      "127_standard_electronic_125_kph", "128_standard_electronic_135_kph",
      "129_standard_electronic_145_kph", "130-?", "131-?", "132-?", "133-?",
      "134-?", "135-?", "136-?", "137-?", "138-?", "139-?", "140-?", "141-?",
      "142-?", "143-?", "144-?", "145-?", "146-?", "147-?", "148-?", "149-?",
      "150-?", "151-?", "152-?", "153-?", "154-?", "155-?", "156-?", "157-?",
      "158-?", "159-?", "160-?", "161-?", "162-?", "163-?", "164-?", "165-?",
      "166-?", "167-?", "168-?", "169-?", "170-?",
      "171_standard_regular_motorWay_begin",
      "172_standard_regular_end_of_fMotorWay",
      "173_standard_regular_expressWay_begin",
      "174_standard_regular_end_of_ExpressWay",
      "175_standard_regular_Playground_area_begin",
      "176_standard_regular_End_of_playground_area", "177-?", "178-?", "179-?",
      "180-?", "181-?", "182-?", "183-?", "184-?", "185-?", "186-?", "187-?",
      "188-?", "189-?", "190-?", "191-?", "192-?", "193-?", "194-?", "195-?",
      "196-?", "197-?", "198-?", "199-?",
      "200_standard_regular_no_passing_start",
      "201_standard_regular_end_of_no_passing", "202-?", "203-?", "204-?",
      "205-?", "206-?", "207-?", "208-?", "209-?", "210-?", "211-?", "212-?",
      "213-?", "214-?", "215-?", "216-?", "217-?", "218-?", "219-?",
      "220_standard_electronic_no_passing_start",
      "221_standard_electronic_end_of_no_passing", "222-?", "223-?", "224-?",
      "225-?", "226-?", "227-?", "228-?", "229-?", "230-?", "231-?", "232-?",
      "233-?", "234-?", "235-?", "236-?", "237-?", "238-?", "239-?", "240-?",
      "241-?", "242-?", "243-?", "244-?", "245-?", "246-?", "247-?", "248-?",
      "249-?", "250-?", "251-?", "252-?", "253-?", "254_No_sign_detected",
      "255_e_invalid_sign"};

  std::vector<std::string> topics;
  topics.push_back(std::string("/ld_can"));
  topics.push_back(std::string("/ld_mobileye"));
  rosbag::View view(bag, rosbag::TopicQuery(topics));

  int data[8];
  int IDdec;
  std::string IDhex;

  ld_msgs::ld_mobileye_lane lane;

  foreach (rosbag::MessageInstance const m,
           view)  // A class pointing into a bag file.
  {
    ld_msgs::ld_can::ConstPtr s = m.instantiate<ld_msgs::ld_can>();
    if (m.getTopic() == "/ld_can") {
      int ID = s->ID;
      std_msgs::Header header = s->header;
      int IDdec = (int)s->ID;
      std::string IDhex = int_to_hex(IDdec);
      if (IDhex == "0x101") {
        ++number;
        bag_write.write("/ld_can", header.stamp, *s);
        //                std::cout << number << '\n';
        //				write_test<<header.stamp.toSec()<<","<<header.seq<<","<<header.stamp<<","<<header.frame_id<<","<<ID<<","<<LEN<<","
        //					<<data[0]<<","<<data[1]<<","<<data[2]<<","<<data[3]<<","<<data[4]<<","<<data[5]<<","<<data[6]<<","<<data[7]<<'\n';
      }
      if (IDhex == "0xfd") {
        ++number;
        bag_write.write("/ld_can", header.stamp, *s);
        //                std::cout << number << '\n';
        //				write_test<<header.stamp.toSec()<<","<<header.seq<<","<<header.stamp<<","<<header.frame_id<<","<<ID<<","<<LEN<<","
        //					<<data[0]<<","<<data[1]<<","<<data[2]<<","<<data[3]<<","<<data[4]<<","<<data[5]<<","<<data[6]<<","<<data[7]<<'\n';
      }
    }
    if (m.getTopic() == "/ld_mobileye") {
      data[0] = (int)s->DATA[0];  //把十进制数据存入int中
      data[1] = (int)s->DATA[1];
      data[2] = (int)s->DATA[2];
      data[3] = (int)s->DATA[3];
      data[4] = (int)s->DATA[4];
      data[5] = (int)s->DATA[5];
      data[6] = (int)s->DATA[6];
      data[7] = (int)s->DATA[7];
      IDdec = (int)s->ID;
      IDhex = int_to_hex(IDdec);  //将十进制ID转为十六进制
      std_msgs::Header mob_header = s->header;
      std::bitset<8> byte0(data[0]);
      std::bitset<8> byte1(data[1]);
      std::bitset<8> byte2(data[2]);
      std::bitset<8> byte3(data[3]);
      std::bitset<8> byte4(data[4]);
      std::bitset<8> byte5(data[5]);
      std::bitset<8> byte6(data[6]);
      std::bitset<8> byte7(data[7]);
      // distance to left and right lane
      if (IDhex == "0x669") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "distance";
        int distance_to_left_lane_ = (data[2] << 4) | (data[1] >> 4);
        lane.distance_to_left_lane = distance_to_left_lane_ * 0.02;
        int distance_to_right_lane_ = (data[7] << 4) | (data[6] >> 4);
        lane.distance_to_right_lane = distance_to_right_lane_ * 0.02;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // Left Lane
      if (IDhex == "0x766") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "left_A";
        double left_c0_ = (-1 * (data[2] & 0x80) << 8) +
                          ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.left_c0 = left_c0_ / 256;
        double left_c2_ori = data[4] << 8 | data[3];
        lane.left_c2 = (left_c2_ori - 32767) / 1024 / 1000;
        double left_c3_ori = (data[6] << 8 | data[5]);
        lane.left_c3 = (left_c3_ori - 32767) / 268435456;
        lane.left_lane_type = data[0] & 0xf;
        lane.left_quality = (data[0] & 0x30) >> 4;
        lane.left_model_degree = (data[0] & 0xc0) >> 6;
        lane.left_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //		////left c1
      if (IDhex == "0x767") {
        ++number;
        lane.header = mob_header;
        lane.type = "left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double left_c1_ = data[1] << 8 | data[0];
        lane.left_c1 = (left_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      ///////////////////////////////////////////////////////////////////////////////
      //		////Right Lane
      if (IDhex == "0x768") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "right_A";
        double right_c0_ = (-1 * (data[2] & 0x80) << 8) +
                           ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.right_c0 = right_c0_ / 256;
        double right_c2_ori = data[4] << 8 | data[3];
        lane.right_c2 = (right_c2_ori - 32767) / 1024 / 1000;
        double right_c3_ori = (data[6] << 8 | data[5]);
        lane.right_c3 = (right_c3_ori - 32767) / 268435456;
        lane.right_lane_type = data[0] & 0xf;
        lane.right_quality = (data[0] & 0x30) >> 4;
        lane.right_model_degree = (data[0] & 0xc0) >> 6;
        lane.right_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //		////right c1
      if (IDhex == "0x769") {
        lane.header = mob_header;
        lane.type = "left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double right_c1_ = data[1] << 8 | data[0];
        lane.right_c1 = (right_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      ////////////////////////////////////////////////////////////////////////////
      // number of next lane markers reported
      if (IDhex == "0x76b") {
        ++number;
        lane.header = mob_header;
        lane.type = "next_number";
        lane.ID = IDdec;
        lane.address = IDhex;
        lane.number_of_next_lane = data[0];
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      ////////////////////////////////////////////////////////////
      //			//next_1_left_A
      if (IDhex == "0x76c") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_1_left_A";
        double next_1_left_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_1_left_c0 = next_1_left_c0_ / 256;
        double next_1_left_c2_ori = data[4] << 8 | data[3];
        lane.next_1_left_c2 = (next_1_left_c2_ori - 32767) / 1024 / 1000;
        double next_1_left_c3_ori = (data[6] << 8 | data[5]);
        lane.next_1_left_c3 = (next_1_left_c3_ori - 32767) / 268435456;
        lane.next_1_left_lane_type = data[0] & 0xf;
        lane.next_1_left_quality = (data[0] & 0x30) >> 4;
        lane.next_1_left_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_1_left_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //		//next_1_left_B
      if (IDhex == "0x76d") {
        ++number;
        lane.header = mob_header;
        lane.type = "next_1_left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_1_left_c1_ = data[1] << 8 | data[0];
        lane.next_1_left_c1 = (next_1_left_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      /////next_1_right_A
      if (IDhex == "0x76e") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_1_right_A";
        double next_1_right_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_1_right_c0 = next_1_right_c0_ / 256;
        double next_1_right_c2_ori = data[4] << 8 | data[3];
        lane.next_1_right_c2 = (next_1_right_c2_ori - 32767) / 1024 / 1000;
        double next_1_right_c3_ori = (data[6] << 8 | data[5]);
        lane.next_1_right_c3 = (next_1_right_c3_ori - 32767) / 268435456;
        lane.next_1_right_lane_type = data[0] & 0xf;
        lane.next_1_right_quality = (data[0] & 0x30) >> 4;
        lane.next_1_right_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_1_right_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_1_right_B
      if (IDhex == "0x76f") {
        lane.header = mob_header;
        lane.type = "next_1_right_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_1_right_c1_ = data[1] << 8 | data[0];
        lane.next_1_right_c1 = (next_1_right_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //////////////////////////////////////////////////////////////
      //				//////next_2_left_A
      if (IDhex == "0x770") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_2_left_A";
        double next_2_left_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_2_left_c0 = next_2_left_c0_ / 256;
        double next_2_left_c2_ori = data[4] << 8 | data[3];
        lane.next_2_left_c2 = (next_2_left_c2_ori - 32767) / 1024 / 1000;
        double next_2_left_c3_ori = (data[6] << 8 | data[5]);
        lane.next_2_left_c3 = (next_2_left_c3_ori - 32767) / 268435456;
        lane.next_2_left_lane_type = data[0] & 0xf;
        lane.next_2_left_quality = (data[0] & 0x30) >> 4;
        lane.next_2_left_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_2_left_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        //			bag_write.write("/ld_mobileye_lane",lane.header.stamp,lane);
      }
      //		//next_2_left_B
      if (IDhex == "0x771") {
        ++number;
        lane.header = mob_header;
        lane.type = "next_2_left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_2_left_c1_ = data[1] << 8 | data[0];
        lane.next_2_left_c1 = (next_2_left_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //				//////next_2_right_A
      if (IDhex == "0x772") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_2_right_A";
        double next_2_right_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_2_right_c0 = next_2_right_c0_ / 256;
        double next_2_right_c2_ori = data[4] << 8 | data[3];
        lane.next_2_right_c2 = (next_2_right_c2_ori - 32767) / 1024 / 1000;
        double next_2_right_c3_ori = (data[6] << 8 | data[5]);
        lane.next_2_right_c3 = (next_2_right_c3_ori - 32767) / 268435456;
        lane.next_2_right_lane_type = data[0] & 0xf;
        lane.next_2_right_quality = (data[0] & 0x30) >> 4;
        lane.next_2_right_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_2_right_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //		//next_2_right_B
      if (IDhex == "0x773") {
        lane.header = mob_header;
        lane.type = "next_2_right_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_2_right_c1_ = data[1] << 8 | data[0];
        lane.next_2_right_c1 = (next_2_right_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      /////////////////////////////////////////////////////////////
      //		//next_3_left_A
      if (IDhex == "0x774") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_3_left_A";
        double next_3_left_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_3_left_c0 = next_3_left_c0_ / 256;
        double next_3_left_c2_ori = data[4] << 8 | data[3];
        lane.next_3_left_c2 = (next_3_left_c2_ori - 32767) / 1024 / 1000;
        double next_3_left_c3_ori = (data[6] << 8 | data[5]);
        lane.next_3_left_c3 = (next_3_left_c3_ori - 32767) / 268435456;
        lane.next_3_left_lane_type = data[0] & 0xf;
        lane.next_3_left_quality = (data[0] & 0x30) >> 4;
        lane.next_3_left_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_3_left_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_3_left_B
      if (IDhex == "0x775") {
        ++number;
        lane.header = mob_header;
        lane.type = "next_3_left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_3_left_c1_ = data[1] << 8 | data[0];
        lane.next_3_left_c1 = (next_3_left_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_3_right_A
      if (IDhex == "0x776") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_3_right_A";
        double next_3_right_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_3_right_c0 = next_3_right_c0_ / 256;
        double next_3_right_c2_ori = data[4] << 8 | data[3];
        lane.next_3_right_c2 = (next_3_right_c2_ori - 32767) / 1024 / 1000;
        double next_3_right_c3_ori = (data[6] << 8 | data[5]);
        lane.next_3_right_c3 = (next_3_right_c3_ori - 32767) / 268435456;
        lane.next_3_right_lane_type = data[0] & 0xf;
        lane.next_3_right_quality = (data[0] & 0x30) >> 4;
        lane.next_3_right_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_3_right_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_3_right_B
      if (IDhex == "0x777") {
        lane.header = mob_header;
        lane.type = "next_3_right_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_3_right_c1_ = data[1] << 8 | data[0];
        lane.next_3_right_c1 = (next_3_right_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      /////////////////////////////////////////////////////////////
      //		//next_4_left_A
      if (IDhex == "0x778") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_4_left_A";
        double next_4_left_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_4_left_c0 = next_4_left_c0_ / 256;
        double next_4_left_c2_ori = data[4] << 8 | data[3];
        lane.next_4_left_c2 = (next_4_left_c2_ori - 32767) / 1024 / 1000;
        double next_4_left_c3_ori = (data[6] << 8 | data[5]);
        lane.next_4_left_c3 = (next_4_left_c3_ori - 32767) / 268435456;
        lane.next_4_left_lane_type = data[0] & 0xf;
        lane.next_4_left_quality = (data[0] & 0x30) >> 4;
        lane.next_4_left_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_4_left_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_4_left_B
      if (IDhex == "0x779") {
        ++number;
        lane.header = mob_header;
        lane.type = "next_4_left_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_4_left_c1_ = data[1] << 8 | data[0];
        lane.next_4_left_c1 = (next_4_left_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //////next_4_right_A
      if (IDhex == "0x77a") {
        ++number;
        lane.address = IDhex;
        lane.header = mob_header;
        lane.type = "next_4_right_A";
        double next_4_right_c0_ =
            (-1 * (data[2] & 0x80) << 8) +
            ((data[2] & 0x7f) << 8 | data[1]);  // signed int
        lane.next_4_right_c0 = next_4_right_c0_ / 256;
        double next_4_right_c2_ori = data[4] << 8 | data[3];
        lane.next_4_right_c2 = (next_4_right_c2_ori - 32767) / 1024 / 1000;
        double next_4_right_c3_ori = (data[6] << 8 | data[5]);
        lane.next_4_right_c3 = (next_4_right_c3_ori - 32767) / 268435456;
        lane.next_4_right_lane_type = data[0] & 0xf;
        lane.next_4_right_quality = (data[0] & 0x30) >> 4;
        lane.next_4_right_model_degree = (data[0] & 0xc0) >> 6;
        lane.next_4_right_width_marking = data[7] * 0.01;
        std::cout << number << '\t' << IDhex << '\n';
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      // next_4_right_B
      if (IDhex == "0x77b") {
        lane.header = mob_header;
        lane.type = "next_4_right_B";
        lane.ID = IDdec;
        lane.address = IDhex;
        double next_4_right_c1_ = data[1] << 8 | data[0];
        lane.next_4_right_c1 = (next_4_right_c1_ - 32767) / 1024;
        bag_write.write("/ld_mobileye_lane", lane.header.stamp, lane);
      }
      //		查看Sign
      if (IDhex == "0x720") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x721") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x722") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x723") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x724") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x725") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }
      if (IDhex == "0x726") {
        ++number;
        ld_msgs::ld_mobileye_traffic_sign sign;
        sign.header = mob_header;
        sign.address = IDhex;
        sign.sign_type = data[0];
        sign.text_sign = treffic_sign[sign.sign_type];
        sign.supplementary_sign_type = data[1];
        sign.text_supplementary_sign_type =
            suppementary_sign(sign.supplementary_sign_type);
        sign.filter_type = data[5];
        sign.position_x = data[2];
        sign.position_y = -1 * (data[3] & 0x80) + data[3] & 0x7f;  // signed int
        sign.position_z = -1 * (data[4] & 0x80) + data[4] & 0x7f;  // signed int
        std::cout << number << '\n';
        bag_write.write("/ld_mobileye_sign", sign.header.stamp, sign);
      }

    }  // get topics
  }    // foreach

  topics.clear();
  topics.shrink_to_fit();
  bag.close();

  bag_write.close();
  //	innocan_value(bagname_write, inno_bagName);
  std::cout << "Step 2 finished" << std::endl;
  std::cout << "----------------------- " << '\n';
  return bagname_write;
}

#endif  // INNOCAN_VEL_H
