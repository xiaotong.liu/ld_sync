/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INNOCAN_SYNC_H
#define INNOCAN_SYNC_H

#include <iostream>
#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

void innocan_sync(std::string bagname_can, std::string bagname_innovusion,
                  std::string bagname_write, const int pc_counter,
                  std::vector<ros::Time>& finalTime,
                  std::vector<sensor_msgs::PointCloud2>& pc_value,
                  std::string iv_bagfilename) {
  std::cout << "Step 5 starts" << std::endl;

  //    const std::string sync_file = opt_path +"/"+ iv_name_tobe_added
  //    +"_sync.bag";
  bagname_write = bagname_write + "/" + "syn_inno_autocan_" + iv_bagfilename;

  //    std::cout << "result file name is " << bagname_write << std::endl;

  /**************** here to change files *************************/
  //	std::string bagname_can =
  //			"/home/chinyen/Documents/syn/4-5.bag";
  //	std::string bagname_innovusion =
  //			"/media/chinyen/IBEOFAT32/chengnuo/20190503130317_16.bag";
  //	std::string bagname_write =
  //			"/home/chinyen/Documents/syn/5-5.bag";
  /**************** here to change files *************************/

  rosbag::Bag bag_can(bagname_can);  // the bag with info of can
  //    std::cout << "CAN bag open: " << bagname_can << std::endl;

  rosbag::Bag bag_inno(bagname_innovusion);  // innovusion
  //	rosbag::Bag bag_imu("/media/han/Transcend/Demo
  // VW/syn/PedAndBike/3_430085337_2.bag"); //imu

  //    std::cout << "innovusion bag open: " << bagname_innovusion << std::endl;

  /**************** here to change files *************************/
  /**************** write:totally rewrite or append: extend from old file
   * *************************/
  rosbag::Bag bag_write(bagname_write,
                        rosbag::bagmode::Write);  // fullly rewrite a new bag
  //	rosbag::Bag bag_write(bagname_write,rosbag::bagmode::Append); // extend
  // the data from the old bag
  /**************** here to change files *************************/

  //    std::cout << "write bag open: " << bagname_write << std::endl;

  std::vector<ld_msgs::ld_imugps_can> can_value;
  std::vector<ros::Time> can_time;

  std::vector<ld_msgs::ld_imugps_can> imu_value;

  std::vector<sensor_msgs::PointCloud2>
      iv_pc_value;  // point cloud from innovusion
  int point_number = 0;

  //    std::cout << "point num " << point_number << std::endl;

  std::vector<std::string> topics;

  std::vector<ros::Time> pc_time;

  topics.push_back(std::string("/iv_points"));
  topics.push_back(std::string("/ld_image"));

  topics.push_back(std::string("/tunnel_label"));
  topics.push_back(std::string("/entrance_exit_label"));
  topics.push_back(std::string("/adjacent_lane_label"));
  topics.push_back(std::string("/kitti/velo/pointcloud"));
  topics.push_back(std::string("/points_ground"));
  topics.push_back(std::string("/lane_points"));

  std::vector<visualization_msgs::MarkerArray::ConstPtr> tunnel;
  std::vector<visualization_msgs::MarkerArray::ConstPtr> exit;
  std::vector<visualization_msgs::MarkerArray::ConstPtr> start;

  std::vector<sensor_msgs::PointCloud2::ConstPtr> kitti_pc_value;
  std::vector<sensor_msgs::PointCloud2::ConstPtr> gd_pc_value;
  std::vector<sensor_msgs::PointCloud2::ConstPtr> lane_pc_value;

  rosbag::View view_inno(bag_inno, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const n, view_inno) {
    if (n.getTopic() ==
        "/ld_imugps_can")  //有时候imu的数据是放在innovusion的包里
    {
      ld_msgs::ld_imugps_can::ConstPtr inno_ptr =
          n.instantiate<ld_msgs::ld_imugps_can>();
      imu_value.push_back(*inno_ptr);
    }
    if (n.getTopic() == "/iv_points") {
      ++point_number;
      sensor_msgs::PointCloud2::ConstPtr v =
          n.instantiate<sensor_msgs::PointCloud2>();

      //            if (v->header.stamp > finalTime.at(0) && v->header.stamp <
      //            finalTime.at(finalTime.size()-1))
      //            {
      //                iv_pc_value.push_back(*v);
      //                //
      //                bag_write.write("/pandar_points",v->header.stamp,*v);
      //                // write can into bag
      //                std::cout<<point_number<<"point"<<'\n';
      //                std::cout<<"iv_pc_value->header.stamp"<<v->header.stamp<<'\n';
      //                pc_time.push_back(v->header.stamp);
      //            }

      iv_pc_value.push_back(*v);
      //            std::cout<<point_number<<"point"<<'\n';
      //            std::cout<<"iv_pc_value->header.stamp"<<v->header.stamp<<'\n';
      pc_time.push_back(v->header.stamp);

      //这里v和*v是一样的？
    }

    if (n.getTopic() == "/ld_image") {
      sensor_msgs::Image::ConstPtr s = n.instantiate<sensor_msgs::Image>();
      //            bag_write.write("/ld_image",s->header.stamp,*s);
    }

    if (n.getTopic() == "/tunnel_label") {
      visualization_msgs::MarkerArray::ConstPtr it =
          n.instantiate<visualization_msgs::MarkerArray>();
      tunnel.push_back(it);
    }

    if (n.getTopic() == "/entrance_exit_label") {
      visualization_msgs::MarkerArray::ConstPtr iex =
          n.instantiate<visualization_msgs::MarkerArray>();
      exit.push_back(iex);
    }

    if (n.getTopic() == "/adjacent_lane_label") {
      visualization_msgs::MarkerArray::ConstPtr iad =
          n.instantiate<visualization_msgs::MarkerArray>();
      start.push_back(iad);
    }

    if (n.getTopic() == "/kitti/velo/pointcloud") {
      sensor_msgs::PointCloud2::ConstPtr v =
          n.instantiate<sensor_msgs::PointCloud2>();
      kitti_pc_value.push_back(v);
    }
    if (n.getTopic() == "/points_ground") {
      sensor_msgs::PointCloud2::ConstPtr v =
          n.instantiate<sensor_msgs::PointCloud2>();
      gd_pc_value.push_back(v);
    }
    if (n.getTopic() == "/lane_points") {
      sensor_msgs::PointCloud2::ConstPtr v =
          n.instantiate<sensor_msgs::PointCloud2>();
      lane_pc_value.push_back(v);
    }
  }
  bag_inno.close();  //要关闭第一个包以后，第二个包的循环才能正常开始

  int can_pushback_nr = 0;

  //    std::cout << "can_pushback_nr: " << can_pushback_nr << std::endl;

  topics.push_back(std::string("/ld_imugps_can"));
  rosbag::View view_can(bag_can, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const m, view_can) {
    ld_msgs::ld_imugps_can::ConstPtr can_ptr =
        m.instantiate<ld_msgs::ld_imugps_can>();
    if (m.getTopic() == "/ld_imugps_can") {
      can_value.push_back(*can_ptr);
      can_time.push_back(pc_time[can_pushback_nr]);
      //			bag_write.write("/ld_imugps_can",can_ptr->header.stamp,can_ptr);
      //            std::cout<<can_pushback_nr<<"can_pushback"<<'\n';
      //            std::cout<<can_time[can_pushback_nr]<<"can_ptr->header.stamp"<<'\n';

      ++can_pushback_nr;
    }
  }
  bag_can.close();
  // throw pc values in which time stamps are earlier than first can msgs away
  iv_pc_value.erase(iv_pc_value.begin(), iv_pc_value.begin() + pc_counter);

  for (int idx_iv = 0; idx_iv < pc_time.size(); idx_iv++) {
    //        std::cout << "time stamp" << pc_time.at(idx_iv) << std::endl;
    bag_write.write("/iv_points", pc_time[idx_iv], pc_value[idx_iv]);
    bag_write.write("/ld_imugps_can", pc_time[idx_iv], can_value[idx_iv]);
    if (tunnel.size() > 0) {
      bag_write.write("/tunnel_label", pc_time[idx_iv], *tunnel[idx_iv]);
      bag_write.write("/entrance_exit_label", pc_time[idx_iv], *exit[idx_iv]);
      bag_write.write("/adjacent_lane_label", pc_time[idx_iv], *start[idx_iv]);
      bag_write.write("/kitti/velo/pointcloud", pc_time[idx_iv],
                      *kitti_pc_value[idx_iv]);
      bag_write.write("/points_ground", pc_time[idx_iv], *gd_pc_value[idx_iv]);
      bag_write.write("/lane_points", pc_time[idx_iv], *lane_pc_value[idx_iv]);
    }
  }

  //    std::cout << "final size" << pc_time.size() << std::endl;

  /*
  std::cout<<"*****now writing can and imu into
  bag*****"<<can_value.size()<<'\n';
  int syn_num=0;			// write can into bag	//
  因为有时候imu的数据在innovusion中，所以要把这个放在最后写，因为imu和can的数据都要写
  std::cout<<syn_num<<'\n';
  int can_value_size=can_value.size();
  for(syn_num; syn_num<1500
  ;++syn_num)//这里要注意，是imu的数据大，还是can的数据大，有时候没有imu，就要用can的数据大小
  {
      imu_value[syn_num].acceleration_x=can_value[syn_num].acceleration_x;
  //如果没有imu，到目前位置imu_value其实是空的，
      imu_value[syn_num].acceleration_y=can_value[syn_num].acceleration_y;
      imu_value[syn_num].yawrate=can_value[syn_num].yawrate;
      imu_value[syn_num].v_x_CAN=can_value[syn_num].v_x_CAN;
      imu_value[syn_num].can_ts=can_value[syn_num].header.stamp;
      std::cout<<imu_value[syn_num].acceleration_x<<'\n';
      bag_write.write("/ld_imugps_can",can_value[syn_num].header.stamp,can_value[syn_num]);
      std::cout<<syn_num<<"syn"<<'\n';
  }
*/

  can_value.clear();
  can_value.shrink_to_fit();

  can_time.clear();
  can_time.shrink_to_fit();

  imu_value.clear();
  imu_value.shrink_to_fit();

  iv_pc_value.clear();  // point cloud from innovusion
  iv_pc_value.shrink_to_fit();

  topics.clear();
  topics.shrink_to_fit();

  pc_time.clear();
  pc_time.shrink_to_fit();

  tunnel.clear();
  tunnel.shrink_to_fit();

  exit.clear();
  exit.shrink_to_fit();

  start.clear();
  start.shrink_to_fit();

  kitti_pc_value.clear();
  kitti_pc_value.shrink_to_fit();

  gd_pc_value.clear();
  gd_pc_value.shrink_to_fit();

  lane_pc_value.clear();
  lane_pc_value.shrink_to_fit();
  std::cout << "Step 5 finished" << '\n';
  std::cout << "--------------------" << std::endl;

  bag_write.close();
}

#endif  // INNOCAN_SYNC_H
