/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef SYNC_AUTOCAN_H
#define SYNC_AUTOCAN_H

class sync_autoCAN {
 public:
  sync_autoCAN();
};

#endif  // SYNC_AUTOCAN_H
