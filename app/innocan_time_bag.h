/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef INNOCAN_TIME_BAG_H
#define INNOCAN_TIME_BAG_H

#include <iostream>
#include <string>
#include <vector>

// libs for rosbag API
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include "std_msgs/String.h"
//#include <sensor_msgs/PointCloud2.h>

#include <ld_msgs/ld_can.h>
#include <ld_msgs/ld_imugps_can.h>

#include <ld_msgs/ld_mobileye_lane.h>
#include <ld_msgs/ld_mobileye_traffic_sign.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

std::string innocan_time_bag(std::string bagname_original_1,
                             std::string bagname_write, int time_start,
                             int time_end,
                             std::string bagname_original_2 = "") {
  std::cout << "Step 1 starts" << std::endl;

  // time_start should be the intiger of the first timestemp of innovusion.bag-1
  // = [timestemp]-1, for example: 1557991337.36 -> 1557991337-1 = 1557991336
  // time_end should be the intiger of the last timestemp of innovusion.bag+1 =
  // [timestemp]+1, for example: 1557991337.36 -> 1557991337+1 = 1557991338

  /**************** here to change files *************************/
  //	bagname_original_1="/home/chinyen/Documents/autocan_20190807160512_00.bag";
  //	bagname_original_2="";
  //	bagname_write="/home/chinyen/Documents/syn/1-5.bag";
  //
  //	int time_start=1565786126;
  //	int time_end=1565786155;
  /**************** here to change files *************************/

  std::cout << "now open the bag\n";

  /*+++  original bag 1  +++*/
  rosbag::Bag bag(bagname_original_1);

  /*+++  original bag 2  +++*/
  /*change here if you want 2-input-bags*/
  /*if you want to push 2 bags in one bag*/

  bool noBag2 = (bagname_original_2 == "");

  //	rosbag::Bag bag_2;
  //	if (!noBag2)
  //	{
  //		bag_2.open(bagname_original_2, rosbag::bagmode::Read);
  //	}

  bagname_write = bagname_write + "/1-5.bag";
  /*+++  the new bag  +++*/
  rosbag::Bag bag_write(bagname_write, rosbag::bagmode::Write);

  std::cout << "the size of the bag is: " << bag.getSize() << '\n';

  int number = 0;
  int data[8];

  // this time should smaller than the required time, required time -1
  // the time of data from /ld_can should be greater than this time
  ros::Time test_time_1(static_cast<ros::Time>(time_start));

  // this time should greater than the required time, required time +1
  // the time of data from /ld_can should be smaller than this time
  ros::Time test_time_2(static_cast<ros::Time>(time_end));

  ld_msgs::ld_can can_write;

  //	std::fstream write_test;
  /*change here*/
  //	write_test.open("/media/han/Transcend/Demo
  // VW/syn/temp_can/t5-time.csv",std::ios::out);
  //	write_test<<"time"<<","<<"fieldheaderseq"<<","<<"fieldheaderstamp"<<","<<"fieldheaderframe_id"<<","<<"fieldid"<<","<<"fieldLEN"<<","
  //			<<"data0"<<","<<"data1"<<","<<"data2"<<","<<"data3"<<","<<"data4"<<","<<"data5"<<","<<"data6"<<","<<"data7"<<'\n';
  std::vector<std::string> topics;
  topics.push_back(std::string("/ld_can"));
  topics.push_back(std::string("/ld_mobileye"));

  /* bag 1 */
  rosbag::View view(bag, rosbag::TopicQuery(topics));
  foreach (rosbag::MessageInstance const m,
           view)  // A class pointing into a bag file.
  {
    ld_msgs::ld_can::ConstPtr s = m.instantiate<ld_msgs::ld_can>();
    //					if (m.getTopic() == "/ld_mobileye")
    if (m.getTopic() == "/ld_can") {
      if (s->header.stamp >= test_time_1 && s->header.stamp <= test_time_2) {
        ++number;
        //							write_test<<header.stamp.toSec()<<","<<header.seq<<","<<header.stamp<<","<<header.frame_id<<","<<ID<<","<<LEN<<","
        //									<<data[0]<<","<<data[1]<<","<<data[2]<<","<<data[3]<<","<<data[4]<<","<<data[5]<<","<<data[6]<<","<<data[7]<<'\n';
        /*---here write the tpoic---*/
        //							bag_write.write("/ld_mobileye",s->header.stamp,*s);
        bag_write.write("/ld_can", s->header.stamp, *s);
        ros::Duration diff = s->header.stamp - test_time_1;
        //                std::cout<<number<<" "<<s->header.stamp<<"
        //                "<<test_time_1<<" "<<test_time_2<<'\n';
      }
    }
  }
  bag.close();

  /* bag 2 */
  /* to put two bags together */
  /* keep bag 2 for mobileye use in the future*/

  if (!noBag2) {
    rosbag::Bag bag_2;
    bag_2.open(bagname_original_2, rosbag::bagmode::Read);

    std::cout << "********Now Bag 2(mobileye)********" << '\n';
    rosbag::View view2(bag_2, rosbag::TopicQuery(topics));
    foreach (rosbag::MessageInstance const m,
             view2)  // A class pointing into a bag file.
    {
      ld_msgs::ld_can::ConstPtr s = m.instantiate<ld_msgs::ld_can>();
      // if (m.getTopic() == "/ld_mobileye")
      if (m.getTopic() == "/ld_can") {
        if (s->header.stamp >= test_time_1 && s->header.stamp <= test_time_2) {
          ++number;
          // bag_write.write("/ld_mobileye",s->header.stamp,*s);
          bag_write.write("/ld_can", s->header.stamp, *s);
          ros::Duration diff = s->header.stamp - test_time_1;
          std::cout << number << " " << s->header.stamp << " " << test_time_1
                    << " " << test_time_2 << '\n';
        }
      }
      if (m.getTopic() == "/ld_mobileye") {
        if (s->header.stamp >= test_time_1 && s->header.stamp <= test_time_2) {
          ++number;
          bag_write.write("/ld_mobileye", s->header.stamp, *s);
          //	        				bag_write.write("/ld_can",s->header.stamp,*s);
          ros::Duration diff = s->header.stamp - test_time_1;
          std::cout << number << " " << s->header.stamp << " " << test_time_1
                    << " " << test_time_2 << '\n';
        }
      }
    }
    /* bag 2 close*/
    bag_2.close();
  }

  topics.clear();
  topics.shrink_to_fit();
  bag_write.close();
  std::cout << "Step 1 finshed" << std::endl;
  std::cout << "----------------------- " << '\n';
  return bagname_write;
}
#endif  // INNOCAN_TIME_BAG_H
