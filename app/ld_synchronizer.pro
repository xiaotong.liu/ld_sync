#TEMPLATE = app
#CONFIG += console c++11
#CONFIG -= app_bundle
#CONFIG -= qt

#SOURCES += \
#        main.cpp \
#    alllistener.cpp \
#    interpolation.cpp \
#    main.cpp \
#    sync_autocan.cpp \
#    transform_coordinates.cpp

#greaterThan(QT_MAJOR_VERSION, 4){
#CONFIG += c++11
#} else {
#QMAKE_CXXFLAGS += -std=c++0x
#}

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/release/ -libeosdk_d
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/debug/ -libeosdk_d
#else:unix: LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/ -libeosdk_d

#INCLUDEPATH += $$PWD/../IbeoSDK5.2.2/src
#DEPENDPATH += $$PWD/../IbeoSDK5.2.2/src


#INCLUDEPATH += $$PWD/../Downloads/boost_1_70_0
#DEPENDPATH += $$PWD/../Downloads/boost_1_70_0

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_thread
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_thread
#else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_thread

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_system
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_system
#else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_system


#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_system
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_system
#else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_date_time

##Eigen
#INCLUDEPATH += /usr/include/eigen3
##Vtk
#INCLUDEPATH += /usr/include/vtk-6.2
#LIBS += /usr/lib/x86_64-linux-gnu/libvtk*.so


#INCLUDEPATH += /usr/include/boost
#LIBS += /usr/lib/x86_64-linux-gnu/libboost_*.so

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/release/ -lpthread
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/debug/ -lpthread
#else:unix: LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/ -lpthread

#INCLUDEPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu
#DEPENDPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu

#INCLUDEPATH += $$PWD/../../../opt/ros/include/opencv-3.3.1-dev
#DEPENDPATH += $$PWD/../../../opt/ros/include/opencv-3.3.1-dev

#LIBS += -L/opt/ros/kinetic/lib/ -lcv_bridge

#INCLUDEPATH += /opt/ros/kinetic/lib/x86_64-linux-gnu
#DEPENDPATH += /opt/ros/kinetic/lib/x86_64-linux-gnu
#unix:!macx: LIBS += -L/opt/ros/kinetic/lib/x86_64-linux-gnu/ -lopencv_imgproc3 -lopencv_core3 -lopencv_highgui3 -lopencv_imgcodecs3 -lopencv_videoio3

##PCL Header
#INCLUDEPATH += /usr/include/pcl-1.7
##PCL Lib
#LIBS   +=  /usr/lib/x86_64-linux-gnu/libpcl_*.so

#INCLUDEPATH += /opt/ros/kinetic/include
#DEPENDPATH += /opt/ros/kinetic/include

#LIBS   += /opt/ros/kinetic/lib/librosbag.so
#LIBS   += /opt/ros/kinetic/lib/librosconsole_bridge.so
#LIBS   += /opt/ros/kinetic/lib/libroscpp.so
#LIBS   += /opt/ros/kinetic/lib/libroscpp_serialization.so
#LIBS   += /opt/ros/kinetic/lib/libcpp_common.so
#LIBS   += /opt/ros/kinetic/lib/librostime.so
#LIBS   += /opt/ros/kinetic/lib/librosbag_storage.so
#LIBS   += /opt/ros/kinetic/lib/libroslz4.so

## auto_ware
#INCLUDEPATH += /home/xiaotongliu/VelodyneAlgorithm/devel/include
#DEPENDPATH += /home/xiaotongliu/VelodyneAlgorithm/devel/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/release/ -lconsole_bridge
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/debug/ -lconsole_bridge
#else:unix: LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/ -lconsole_bridge

#INCLUDEPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu
#DEPENDPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu

# #ld_msgs/ld_can

#INCLUDEPATH += /home/xiaotongliu/CANMessageParser/devel/include
#DEPENDPATH += /home/xiaotongliu/CANMessageParser/devel/include

#HEADERS += \
#    alllistener.h \
#    checkarguments.h \
#    FileNamesSort.h \
#    generalfuncs.h \
#    get_times.h \
#    getallformatfiles.h \
#    idc2bag_file_demo.h \
#    innocan_sync.h \
#    innocan_time_bag.h \
#    innocan_value.h \
#    innocan_vel.h \
#    interpolation.h \
#    interpolation_can.h \
#    sync_autocan.h \
#    sync_idc.h \
#    transform_coordinates.h

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    sync_autocan.cpp \
    transform_coordinates.cpp \
    interpolation.cpp

HEADERS += \
    FileNamesSort.h \
    alllistener.h \
    sync_idc.h \
    sync_autocan.h \
    transform_coordinates.h \
    interpolation.h \
    checkarguments.h \
    get_times.h \
    idc2bag_file_demo.h \
    getallformatfiles.h \
    innocan_time_bag.h \
    innocan_vel.h \
    generalfuncs.h \
    innocan_value.h \
    interpolation_can.h \
    innocan_sync.h
greaterThan(QT_MAJOR_VERSION, 4){
CONFIG += c++11
} else {
QMAKE_CXXFLAGS += -std=c++0x
}


# win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/release/ -libeosdk_d
# else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/debug/ -libeosdk_d
# else:unix: LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/ -libeosdk_d

# INCLUDEPATH += $$PWD/../IbeoSDK5.2.2/src
# DEPENDPATH += $$PWD/../IbeoSDK5.2.2/src


INCLUDEPATH += $$PWD/../Downloads/boost_1_70_0
DEPENDPATH += $$PWD/../Downloads/boost_1_70_0

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_thread
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_thread
else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_thread

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_system
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_system
else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_system


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/release/ -lboost_system
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/debug/ -lboost_system
else:unix: LIBS += -L$$PWD/../Downloads/boost_1_70_0/stage/lib/ -lboost_date_time



#Boost
INCLUDEPATH += /usr/include/boost
LIBS += /usr/lib/x86_64-linux-gnu/libboost_*.so

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/release/ -lpthread
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/debug/ -lpthread
else:unix: LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/ -lpthread

INCLUDEPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu

INCLUDEPATH += $$PWD/../../../opt/ros/include/opencv-3.3.1-dev
DEPENDPATH += $$PWD/../../../opt/ros/include/opencv-3.3.1-dev



# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/release/ -libeosdk_d
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/debug/ -libeosdk_d
#else:unix: LIBS += -L$$PWD/../IbeoSDK5.2.2/libs/ -libeosdk_d

INCLUDEPATH += $$PWD/../Downloads/boost_1_70_0/stage
DEPENDPATH += $$PWD/../Downloads/boost_1_70_0/stage



#Eigen
INCLUDEPATH += /usr/include/eigen3
#Vtk
INCLUDEPATH += /usr/include/vtk-6.2
LIBS += /usr/lib/x86_64-linux-gnu/libvtk*.so
#LIBS += /usr/lib/libvtk*.so

#INCLUDEPATH += /usr/local/include
#LIBS += /usr/local/lib/libjpeg.so


#PCL Header
INCLUDEPATH += /usr/include/pcl-1.7
#PCL Lib
LIBS   +=  /usr/lib/x86_64-linux-gnu/libpcl_*.so

INCLUDEPATH += /opt/ros/kinetic/include
DEPENDPATH += /opt/ros/kinetic/include

INCLUDEPATH += /opt/ros/kinetic/lib
DEPENDPATH += /opt/ros/kinetic/lib


INCLUDEPATH += /opt/ros/kinetic/include/opencv-3.3.1-dev
DEPENDPATH += /opt/ros/kinetic/include/opencv-3.3.1-dev

#:-1: error: output.o: undefined reference to symbol '_ZN14console_bridge3logEPKciNS_8LogLevelES1_z'
#LIBS += -L/usr/lib/x86_64-linux-gnu/libconsole_bridge.so
#LIBS += -L/usr/lib/x86_64-linux-gnu/libconsole_bridge.so.0.2
#INCLUDEPATH += /usr/lib/x86_64-linux-gnu
#DEPENDPATH += /usr/lib/x86_64-linux-gnu


#LIBS += /opt/ros/kinetic/lib/libjsk_recognition_utils.so


LIBS += -L/opt/ros/kinetic/lib/ -lcv_bridge

INCLUDEPATH += /opt/ros/kinetic/lib/x86_64-linux-gnu
DEPENDPATH += /opt/ros/kinetic/lib/x86_64-linux-gnu
unix:!macx: LIBS += -L/opt/ros/kinetic/lib/x86_64-linux-gnu/ -lopencv_imgproc3 -lopencv_core3 -lopencv_highgui3 -lopencv_imgcodecs3 -lopencv_videoio3

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Desktop/kinetic/lib/x86_64-linux-gnu/release/ -lopencv_video3
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Desktop/kinetic/lib/x86_64-linux-gnu/debug/ -lopencv_video3
else:unix: LIBS += -L$$PWD/../Desktop/kinetic/lib/x86_64-linux-gnu/ -lopencv_video3

#-------------------------------------------------
#
# Project created by QtCreator 2019-03-01T16:10:33
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#TARGET = rosbag_read
#TEMPLATE = app
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



#LIBS += /usr/lib/libvtk*.so



#LIBS   += /opt/ros/kinetic/include/sensor_msgs/PointCloud2.h
LIBS   += /opt/ros/kinetic/lib/librosbag.so
LIBS   += /opt/ros/kinetic/lib/librosconsole_bridge.so
LIBS   += /opt/ros/kinetic/lib/libroscpp.so
LIBS   += /opt/ros/kinetic/lib/libroscpp_serialization.so
LIBS   += /opt/ros/kinetic/lib/libcpp_common.so
LIBS   += /opt/ros/kinetic/lib/librostime.so



LIBS   += /opt/ros/kinetic/lib/librosbag_storage.so
LIBS   += /opt/ros/kinetic/lib/libroslz4.so

# auto_ware
INCLUDEPATH += /home/xiaotongliu/VelodyneAlgorithm/devel/include
DEPENDPATH += /home/xiaotongliu/VelodyneAlgorithm/devel/include

#INCLUDEPATH += /home/lddev001/VelodyenAlgorithm/devel/include
#DEPENDPATH += /home/lddev001/VelodyenAlgorithm/devel/include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


#:-1: error: output.o: undefined reference to symbol '_ZN14console_bridge3logEPKciNS_8LogLevelES1_z'

#INCLUDEPATH += /usr/lib/x86_64-linux-gnu
#DEPENDPATH += /usr/lib/x86_64-linux-gnu
#LIBS += -L/usr/lib/x86_64-linux-gnu/libconsole_bridge.so
#LIBS += -L/usr/lib/x86_64-linux-gnu/libconsole_bridge.so.0.2
#LIBS += -L/usr/lib/x86_64-linux-gnu/libconsole_bridge.so.0.3


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/release/ -lconsole_bridge
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/debug/ -lconsole_bridge
else:unix: LIBS += -L$$PWD/../../../usr/lib/x86_64-linux-gnu/ -lconsole_bridge

INCLUDEPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../usr/lib/x86_64-linux-gnu

 #ld_msgs/ld_can

INCLUDEPATH += /home/xiaotongliu/CANMessageParser/devel/include
DEPENDPATH += /home/xiaotongliu/CANMessageParser/devel/include
## Qt
#INCLUDEPATH += /home/chinyen/Qt5.11.2/5.11.2/Src/qtbase/src
#DEPENDPATH += /home/chinyen/Qt5.11.2/5.11.2/Src/qtbase/src

#INCLUDEPATH += /home/chinyen/idc2bag/portable-file-dialogs-master
#DEPENDPATH += /home/chinyen/idc2bag/portable-file-dialogs-master

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../opt/cpp_libs/ibeosdk5_2_2/lib/release/ -libeosdk5_2_2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../opt/cpp_libs/ibeosdk5_2_2/lib/debug/ -libeosdk5_2_2
else:unix: LIBS += -L$$PWD/../../../../opt/cpp_libs/ibeosdk5_2_2/lib/ -libeosdk5_2_2

INCLUDEPATH += $$PWD/../../../../opt/cpp_libs/ibeosdk5_2_2/include
DEPENDPATH += $$PWD/../../../../opt/cpp_libs/ibeosdk5_2_2/include


