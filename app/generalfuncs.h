/*!
@file filenamesort.h
@author xiaotong liu
@date 2020/01/10
@copydoc copyright
@brief ibeo listener
*/
#ifndef GENERALFUNCS_H
#define GENERALFUNCS_H

#include <iostream>
#include <sstream>

template <typename T>
std::string int_to_hex(T i) {
  std::stringstream stream;
  stream << "0x" << std::hex << i;
  return stream.str();
}

double yawrate_calculate(int& yawrate_vz, double& yawrate_without_vz) {
  switch (yawrate_vz) {
    case 0:
      return yawrate_without_vz;
    case 1:
      return -1 * yawrate_without_vz;
  }
}

double kmph2mps(double& kmph) { return kmph * 1000 / 3600; }

double degree2radian(double& degree) {
  const double kPi = 3.1415926;
  return degree / 180 * kPi;
}

template <typename T>
std::string ToString(T val) {
  std::stringstream stream;
  stream << val;
  return stream.str();
}

const char* suppementary_sign(const int& sign_num) {
  switch (sign_num) {
    case 0:
      return "None";
    case 1:
      return "Rain";
    case 2:
      return "Snow";
    case 3:
      return "Trailer";
    case 4:
      return "Time";
    case 5:
      return "Arrow_left";
    case 6:
      return "Arrow_right";
    case 7:
      return "BendArrow_left";
    case 8:
      return "BendArrow_right";
    case 9:
      return "Truck";
    case 10:
      return "Distance_arrow";
    case 11:
      return "Weight";
    case 12:
      return "Distance_in";
    case 13:
      return "Tractor";
    case 14:
      return "Snow_rain";
    case 15:
      return "School";
    case 16:
      return "Rain_cloud";
    case 17:
      return "Fog";
    case 18:
      return "Hazardous_materials";
    case 19:
      return "Night";
    case 20:
      return "Supp_sign_generic";
    case 21:
      return "e_rappel";
    case 22:
      return "e_zone";
    case 225:
      return "Invalid_supp";
  }
}
#endif  // GENERALFUNCS_H
